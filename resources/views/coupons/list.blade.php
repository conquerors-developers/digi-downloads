@extends($layout)
@section('header_scripts')
<link href="{{CSS}}ajax-datatables.css" rel="stylesheet">
@stop
@section('content')

 <!-- Content Header (Page header) -->
 <div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><a href="{{PREFIX}}"><i class="fa fa-dashboard"></i> {{ getPhrase('Home') }}</a> </li>
			<li class="active">{{isset($title) ? $title : ''}}</li>
		</ol>
	</div>
</div>

      <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
			<a href="{{URL_COUPONS_ADD}}" class="btn btn-primary pull-right">{{ getPhrase('Add') }}</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover datatable">
                <thead>
                <tr>
					<th>{{ getPhrase('Title') }}</th>
					<th>{{ getPhrase('code') }}</th>
					<th>{{ getPhrase('value') }}</th>
					
					<th>{{ getPhrase('start_date') }}</th>
					<th>{{ getPhrase('end_date') }}</th>
					
					<th>{{ getPhrase('Status') }}</th>
					<th>{{ getPhrase('Action') }}</th>
                </tr>
                </thead>
                <tbody>
                
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
 
           
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

 @endsection
 
@section('footer_scripts')
	@include('common.datatables',array('route' => URL_COUPONS_LIST,'route_as_url'=>TRUE)) 
	@include('common.deletescript', array('route'=>URL_COUPONS_DELETE))
@stop
