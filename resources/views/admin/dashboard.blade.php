@extends($layout)

@section('content')
 <section class="content-header">
      <div class="row">
  <div class="col-lg-12">
    <ol class="breadcrumb">
      <li><a href="{{URL_DASHBOARD}}"><i class="fa fa-home"></i> {{ getPhrase('home') }}</a> </li>
     
  </div>
</div>
</section>
     <!-- Main content -->
    <section class="content">

    <div id="page-wrapper">
      <div class="container-fluid">
      <div class="col-md-3">
            <div class="card card-green text-xs-center">
              <div class="card-block">
            <h4 class="card-title">
              <h4 class="card-title">
                <i class="fa fa-random"></i>
            </h4>

                <p class="card-text">{{ getPhrase('categories')}}</p>
              </div>
              <a class="card-footer text-muted" 
              href="{{URL_CATEGORIES_DASHBOARD}}">
                {{ getPhrase('view_all')}}
              </a>
            </div>
          </div>

           <div class="col-md-3 ">
            <div class="card card-red text-xs-center">
              <div class="card-block">
            <h4 class="card-title">
              <h4 class="card-title">
               <i class="fa fa-product-hunt" aria-hidden="true"></i>

            </h4>

                <p class="card-text">{{ getPhrase('products')}}</p>
              </div>
              <a class="card-footer text-muted" 
              href="{{URL_PRODUCTS_DASHBOARD}}">
                {{ getPhrase('view_all')}}
              </a>
            </div>
          </div>

           <div class="col-md-3 ">
            <div class="card card-blue text-xs-center">
              <div class="card-block">
            <h4 class="card-title">
              <h4 class="card-title">
              <i class="fa fa-hashtag"></i> 
            </h4>

                <p class="card-text">{{ getPhrase('coupons')}}</p>
              </div>
              <a class="card-footer text-muted" 
              href="{{URL_COUPONS_DASHBOARD}}">
                {{ getPhrase('view_all')}}
              </a>
            </div>
          </div>

           <div class="col-md-3 ">
            <div class="card card-black text-xs-center">
              <div class="card-block">
            <h4 class="card-title">
              <h4 class="card-title">
              <i class="fa fa-file-text-o" aria-hidden="true"></i>

            </h4>

                <p class="card-text">{{ getPhrase('pages')}}</p>
              </div>
              <a class="card-footer text-muted" 
              href="{{URL_PAGES_DASHBOARD}}">
                {{ getPhrase('view_all')}}
              </a>
            </div>
          </div>

           <div class="col-md-3 ">
            <div class="card card-yellow text-xs-center">
              <div class="card-block">
            <h4 class="card-title">
              <h4 class="card-title">
               <i class="fa fa-users" aria-hidden="true"></i>
            </h4>

                <p class="card-text">{{ getPhrase('users')}}</p>
              </div>
              <a class="card-footer text-muted" 
              href="{{URL_ADMIN_USERS_DASHBOARD}}">
                {{ getPhrase('view_all')}}
              </a>
            </div>
          </div>

           <div class="col-md-3 ">
            <div class="card card-blue text-xs-center">
              <div class="card-block">
            <h4 class="card-title">
              <h4 class="card-title">
               <i class="fa fa-cog" aria-hidden="true"></i>
            </h4>

                <p class="card-text">{{ getPhrase('settings')}}</p>
              </div>
              <a class="card-footer text-muted" 
              href="{{URL_USERS."all"}}">
                {{ getPhrase('view_all')}}
              </a>
            </div>
          </div>

          <div class="col-md-3 ">
            <div class="card card-green text-xs-center helper_step10">
              <div class="card-block">
              <h4 class="card-title">
                             {{ getPhrase('payment_reports')}}                                  
                             </h4>
              

              </div>
            <a class="card-footer text-muted" 
              href="{{URL_USERS_ADD}}">
                {{ getPhrase('view_all')}}
              </a>
              
            </div>
          </div>
      </div> 
   </div>
    </section>
    <!-- /.content -->
@endsection