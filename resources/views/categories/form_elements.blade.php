     <div class="box-body">
       <div class="col-md-6">
                
				<div class="form-group">
                 {{ Form::label('title', getPhrase( 'Title' ) ) }} {!! required_field(); !!}
                 {{ Form::text('title', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase( 'Eg: Wordpress Templates' ),
						)) }}

                </div>
                
				<div class="form-group">
              	{{ Form::label('description', getPhrase( 'Description' ) ) }}               
				{{ Form::textarea('description', $value = null, $attributes = array('class'=>'form-control', 'placeholder' => getPhrase( 'Enter purpose of the category' ), 'rows'=>'4')) }}
                </div>

                  <div class="form-group">
                 {{ Form::label('meta_tag_title', getPhrase( 'Meta tag title' ) ) }}
                 {{ Form::text('meta_tag_title', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase( 'Meta tag title' ) ) ) }}

                </div>

                  <div class="form-group">
                 {{ Form::label('meta_tag_description', getPhrase( 'Meta Description' ) ) }}
                 {{ Form::textarea('meta_tag_description', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase( 'Meta Description' ),'rows'=>'4'
            )) }}

                </div> 
               <div class="form-group">
                 {{ Form::label('meta_tag_keywords', getPhrase( 'Meta Keywords' ) ) }}
                 {{ Form::text('meta_tag_keywords', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase( 'Meta Keywords' ),
            )) }}

                </div>
               
            </div>

            <div class="col-md-6">
              <div class="form-group">
                 {{ Form::label('Sort Order', getPhrase( 'Sort Order' ) ) }}
                 {{ Form::number('sort_order', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase( 'Eg: Soft Order' ),)) }}
                </div>
				
			  <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                 {{ Form::label('image', getPhrase( 'Image' ) ) }}
                 {{ Form::file('image') }}

                </div>  
              </div>
             
             
			  <div class="col-md-6">               
			   
			   @if( isset($record) && isset( $record->image ) && $record->image != '' )
                <img src="{{IMAGE_URL_CATEGORIES_THUMBNAIL.$record->image}}" alt="">
                @endif
              </div>
			  
              </div>
                
            <div class="form-group">
                 {{ Form::label('show_in_menu', getPhrase( 'Show in menu' ) ) }}
                 {{  Form::checkbox('show_in_menu', '') }}

                </div>
           
               
                <div class="form-group">
                {{ Form::label('parent_id', getPhrase( 'Select Parent' ) ) }}
               <?php
			   $selected = 0;
			   if($record)
				   $selected = $record->parent_id;
			   ?>
				{{Form::select('parent_id', $parent_categories, $selected, ['class'=>'form-control', "id"=>"parent_id"])}}
                </div>
  

          <?php 
              $status[1] = 'Active';
              $status[0] = 'Inactive';

          ?>
                <div class="form-group">
                {{ Form::label('status', getPhrase( 'Select' ) ) }}
               
        {{Form::select('status', $status, null, ['class'=>'form-control', "id"=>"status"])}}
                </div>
           
			</div>
              </div>   
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">{{$button_name}}</button>
              </div>