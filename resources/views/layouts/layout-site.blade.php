<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Home Page for Business Startup</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="Business Tips" content="BUSINESS STARTUP">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ SITE_CSS }}main.css">
	<link href="{{CSS}}sweetalert.css" rel="stylesheet" type="text/css">
	@yield('header_scripts')
</head>

<body ng-app="vehicle_booking">

    <!-- PRELOADER -->
    <div id="preloader">
        <div id="status">
            <div class="mul8circ1"></div>
            <div class="mul8circ2"></div>
        </div>
    </div>
    <!-- /PRELOADER -->

    <!-- Trigger the modal with a button -->
    <!--for side button contact-us-->
    <div class="button-side">
        <button type="button" class="btn btn-default contact-btn" id="side">CONTACT US</button>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">

                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4> CONTACT US</h4>
                    <h6>At solemn va esser necessi fa uniform</h6>
                    <form>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Your Name">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Email Address">
                        </div>
                        <div class="input-group">

                            <select class="selectpicker" title="Subject">
                                <option>Select</option>
                                <option>Mustard</option>
                                <option>Ketchup</option>
                                <option>Relish</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <textarea class="form-control" rows="3" id="comment">Enter Message</textarea>
                        </div>
                        <button type="submit" class="btn btn-success">SEND</button>
                    </form>
                </div>

            </div>

        </div>
    </div>
	
    @include('layouts.includes.layout-site-navigation')
	
	@yield('content')

    @include('layouts.includes.layout-site-footer')

    <!--for back to top button-->
    <a id="back-to-top" href="#" class="back-to-top"> <i class="fa fa-angle-up"></i> </a>
    <!-- Script files-->

    <script type="text/javascript" src="{{ SITE_JS }}jquery.min.js"></script>
    <script type="text/javascript" src="{{ SITE_JS }}bootstrap.min.js"></script>
    <script type="text/javascript" src="{{ SITE_JS }}custom.js"></script>
	
	<script src="{{JS}}sweetalert-dev.js"></script>

	@include('errors.formMessages')
	
@yield('footer_scripts')
</body>

</html>