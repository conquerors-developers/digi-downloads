	<section class="sidebar">
  <!-- Sidebar user panel -->
  <div class="user-panel">
	<div class="pull-left image">
	  <img src="{{ASSETS}}dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
	</div>
	<div class="pull-left info">
	  <p>{{ucfirst($current_user->name)}}</p>
	  <a href="#"><i class="fa fa-circle text-success"></i> {{ getPhrase('Online') }}</a>
	</div>
  </div>
  
  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu">
	
	<li class="{{ isActive($main_active, 'dashboard') }}"><a href="{{URL_DASHBOARD}}"><i class="fa fa-home"></i> {{ getPhrase('Dashboard') }}</a></li>
	<li class="{{ isActive($main_active, 'users') }} treeview">
	  <a href="#">
		<i class="fa fa-users"></i> <span>{{ getPhrase('users') }}</span>
		<span class="pull-right-container">
		  <i class="fa fa-angle-left pull-right"></i>
		</span>
	  </a>
	  <ul class="treeview-menu">
		<li class="{{ isActive($sub_active, 'list') }}"><a href="{{URL_USERS."all"}}"><i class="fa fa-circle-o"></i> {{ getPhrase('all') }}</a></li>
		<li class="{{ isActive($sub_active, 'ownerlist') }}"><a href="{{URL_USERS."owner"}}"><i class="fa fa-circle-o"></i> {{ getPhrase('owners') }}</a></li>
		<li class="{{ isActive($sub_active, 'adminlist') }}"><a href="{{URL_USERS."admin"}}"><i class="fa fa-circle-o"></i> {{ getPhrase('admins') }}</a></li>
		<li class="{{ isActive($sub_active, 'executivelist') }}"><a href="{{URL_USERS."executive"}}"><i class="fa fa-circle-o"></i> {{ getPhrase('executives') }}</a></li>
		<li class="{{ isActive($sub_active, 'vendorlist') }}"><a href="{{URL_USERS."vendor"}}"><i class="fa fa-circle-o"></i> {{ getPhrase('vendors') }}</a></li>
		<li class="{{ isActive($sub_active, 'userlist') }}"><a href="{{URL_USERS."user"}}"><i class="fa fa-circle-o"></i> {{ getPhrase('customers') }}</a></li>
		<li class="{{ isActive($sub_active, 'add') }}"><a href="{{URL_USERS_ADD}}"><i class="fa fa-circle-o"></i> {{ getPhrase('add') }}</a></li>
		<li class="{{ isActive($sub_active, 'import') }}"><a href="{{URL_IMPORT . 'user'}}"><i class="fa fa-circle-o"></i> {{ getPhrase('import') }}</a></li>
	  </ul>
	</li>
	
	<li class="{{ isActive($main_active, 'categories') }} treeview">
	  <a href="#">
		  <i class="fa fa-random"></i> <span>{{ getPhrase('Categories') }}</span>
		<span class="pull-right-container">
		  <i class="fa fa-angle-left pull-right"></i>
		</span>
	  </a>
	  <ul class="treeview-menu">
		<li class="{{ isActive($sub_active, 'list') }}"><a href="{{URL_CATEGORIES}}"><i class="fa fa-circle-o"></i> {{ getPhrase('List') }}</a></li>
		<li class="{{ isActive($sub_active, 'add') }}"><a href="{{URL_CATEGORIES_ADD}}"><i class="fa fa-circle-o"></i> {{ getPhrase('Add') }}</a></li>
		<li class="{{ isActive($sub_active, 'import') }}"><a href="{{URL_IMPORT.'category'}}"><i class="fa fa-circle-o"></i> {{ getPhrase('Import') }}</a></li>
	  </ul>
	</li>
	
	<li class="{{ isActive($main_active, 'coupons') }} treeview">
	  <a href="#">
		<i class="fa fa-hashtag"></i>  <span>{{ getPhrase('coupons') }}</span>
		<span class="pull-right-container">
		  <i class="fa fa-angle-left pull-right"></i>
		</span>
	  </a>
	  <ul class="treeview-menu">
		<li class="{{ isActive($sub_active, 'list') }}"><a href="{{ URL_COUPONS }}"><i class="fa fa-circle-o"></i> {{ getPhrase('List') }}</a></li>
		<li class="{{ isActive($sub_active, 'add') }}"><a href="{{ URL_COUPONS_ADD }}"><i class="fa fa-circle-o"></i> {{ getPhrase('Add') }}</a></li>
	  </ul>
	</li>
	
	<li class="{{ isActive($main_active, 'licences') }} treeview">
	  <a href="#">
		<i class="fa fa-hashtag"></i>  <span>{{ getPhrase('licences') }}</span>
		<span class="pull-right-container">
		  <i class="fa fa-angle-left pull-right"></i>
		</span>
	  </a>
	  <ul class="treeview-menu">
		<li class="{{ isActive($sub_active, 'list') }}"><a href="{{ URL_LICENCES }}"><i class="fa fa-circle-o"></i> {{ getPhrase('List') }}</a></li>
		<li class="{{ isActive($sub_active, 'add') }}"><a href="{{ URL_LICENCES_ADD }}"><i class="fa fa-circle-o"></i> {{ getPhrase('Add') }}</a></li>
	  </ul>
	</li>
	
	<li class="{{ isActive($main_active, 'products') }} treeview">
	  <a href="#">
	  <i class="fa fa-product-hunt" aria-hidden="true"></i> <span>{{ getPhrase('products') }}</span>
		<span class="pull-right-container">
		  <i class="fa fa-angle-left pull-right"></i>
		</span>
	  </a>
	  <ul class="treeview-menu">
		<li class="{{ isActive($sub_active, 'list') }}"><a href="{{ URL_PRODUCTS }}"><i class="fa fa-circle-o"></i> {{ getPhrase('List') }}</a></li>
		<li class="{{ isActive($sub_active, 'add') }}"><a href="{{ URL_PRODUCTS_ADD }}"><i class="fa fa-circle-o"></i> {{ getPhrase('Add') }}</a></li>
		<li class="{{ isActive($sub_active, 'import') }}"><a href="{{ URL_IMPORT . 'product' }}"><i class="fa fa-circle-o"></i> {{ getPhrase('Import') }}</a></li>
	  </ul>
	</li>	
	
	<li class="{{ isActive($main_active, 'templates') }} treeview">
	  <a href="#">
		<i class="fa fa-commenting-o" aria-hidden="true"></i><span>{{ getPhrase('Templates') }}</span>
		<span class="pull-right-container">
		  <i class="fa fa-angle-left pull-right"></i>
		</span>
	  </a>
	  <ul class="treeview-menu">
		<li class="{{ isActive($sub_active, 'list') }}"><a href="{{ URL_TEMPLATES }}"><i class="fa fa-circle-o"></i> List</a></li>
		<li class="{{ isActive($sub_active, 'add') }}"><a href="{{ URL_TEMPLATES_ADD }}"><i class="fa fa-circle-o"></i> Add</a></li>
		
		<li class="{{ isActive($sub_active, 'list_email') }}"><a href="{{ URL_TEMPLATES_EMAIL }}"><i class="fa fa-circle-o"></i> {{ getPhrase('List Email Templates') }}</a></li>
		<li class="{{ isActive($sub_active, 'list_sms') }}"><a href="{{ URL_TEMPLATES_SMS }}"><i class="fa fa-circle-o"></i> {{ getPhrase('List SMS Templates') }}</a></li>
	  </ul>
	</li>
	
	<li class="{{ isActive($main_active, 'offers') }} treeview">
	  <a href="#">
		 <i class="fa fa-file-text-o" aria-hidden="true"></i> <span>{{ getPhrase('offers') }}</span>
		<span class="pull-right-container">
		  <i class="fa fa-angle-left pull-right"></i>
		</span>
	  </a>
	  <ul class="treeview-menu">
		<li class="{{ isActive($sub_active, 'list') }}"><a href="{{ URL_OFFERS }}"><i class="fa fa-circle-o"></i> {{ getPhrase('List') }}</a></li>
		<li class="{{ isActive($sub_active, 'add') }}"><a href="{{ URL_OFFERS_ADD }}"><i class="fa fa-circle-o"></i> {{ getPhrase('Add') }}</a></li>
	  </ul>
	</li>
	
	<li class="{{ isActive($main_active, 'pages') }} treeview">
	  <a href="#">
		 <i class="fa fa-file-text-o" aria-hidden="true"></i> <span>{{ getPhrase('pages') }}</span>
		<span class="pull-right-container">
		  <i class="fa fa-angle-left pull-right"></i>
		</span>
	  </a>
	  <ul class="treeview-menu">
		<li class="{{ isActive($sub_active, 'list') }}"><a href="{{ URL_PAGES }}"><i class="fa fa-circle-o"></i> {{ getPhrase('List') }}</a></li>
		<li class="{{ isActive($sub_active, 'add') }}"><a href="{{ URL_PAGES_ADD }}"><i class="fa fa-circle-o"></i> {{ getPhrase('Add') }}</a></li>
	  </ul>
	</li>
	
	<li class="{{ isActive($main_active, 'reports') }} treeview">
	  <a href="#">
		<i class="fa fa-flag-o" aria-hidden="true"></i>
 <span>{{ getPhrase('payment_reports') }}</span>
		<span class="pull-right-container">
		  <i class="fa fa-angle-left pull-right"></i>
		</span>
	  </a>
	  <ul class="treeview-menu">
		<li class="{{ isActive($sub_active, 'online_reports') }}"><a href="{{ URL_ONLINE_PAYMENT_REPORTS }}"><i class="fa fa-circle-o"></i> {{ getPhrase('online_reports') }}</a></li>

		<li {{ isActive($sub_active, 'offline_reports') }}> <a href="{{URL_OFFLINE_PAYMENT_REPORTS}}">
				<i class="fa fa-circle-o" aria-hidden="true"></i> {{ getPhrase('offline_reports') }} </a> </li>

        <li {{ isActive($sub_active, 'export') }}> <a href="{{URL_LANGUAGES_LIST}}">
				<i class="fa fa-circle-o" aria-hidden="true"></i> {{ getPhrase('export') }} </a> </li>				
	  </ul>
	</li>
	
	<li class="{{ isActive($main_active, 'menu') }} treeview">
	  <a href="#">
		 <i class="fa fa-file-text-o" aria-hidden="true"></i> <span>{{ getPhrase('Menus') }}</span>
		<span class="pull-right-container">
		  <i class="fa fa-angle-left pull-right"></i>
		</span>
	  </a>
	  <ul class="treeview-menu">
		<li class="{{ isActive($sub_active, 'list') }}"><a href="{{ URL_MENU }}"><i class="fa fa-circle-o"></i> {{ getPhrase('List') }}</a></li>
		<li class="{{ isActive($sub_active, 'add') }}"><a href="{{ URL_MENU_ADD }}"><i class="fa fa-circle-o"></i> {{ getPhrase('Add') }}</a></li>
	  </ul>
	</li>

	<li class="{{ isActive($main_active, 'settings') }} treeview">
	  <a href="#">
		 <i class="fa fa-cog" aria-hidden="true"></i> <span>{{ getPhrase('settings') }}</span>
		<span class="pull-right-container">
		  <i class="fa fa-angle-left pull-right"></i>
		</span>
	  </a>
	  <ul class="treeview-menu">
		<li class="{{ isActive($sub_active, 'list') }}"><a href="{{ URL_MASTERSETTINGS_SETTINGS }}"><i class="fa fa-circle-o"></i> {{ getPhrase('List') }}</a></li>

		<li {{ isActive($sub_active, 'languages') }}> <a href="{{URL_LANGUAGES_LIST}}">
				<i class="fa fa-language" aria-hidden="true"></i> {{ getPhrase('languages') }} </a> </li>
	  </ul>
	</li>

	 
 
	 
	 
	  </ul>
	</li>
	
	
	
  </ul>

</section>