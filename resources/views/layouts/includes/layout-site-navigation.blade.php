<?php
if( ! isset($main_active) )
	$main_active = 'login';
?>
<!--HEADER-->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand pw-navbar-brand" href="{{ PREFIX }}">
			<?php
			$site_logo = getSetting('site_logo', 'site_settings');
			if( $site_logo != '' && File::exists( IMAGE_PATH_SETTINGS . $site_logo ) ) {
				$site_logo = IMAGE_PATH_SETTINGS . $site_logo;
			} else {
				$site_logo =  DEFAULT_SITELOGO_IMAGE;
			}
			?>
			<img src="{{ $site_logo }}" alt="" class="img-responsive"></a>
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar1">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<!--<ul class="nav navbar-nav">
				<li><img src="images/logo.png" alt="image missing" class="img-responsive"></li>
			</ul>-->

		<div class="collapse navbar-collapse" id="myNavbar1">
			<?php
			$menu = App\Menu::select(['menus.name','menu_items.*'])->join('menu_items', 'menus.id', '=', 'menu_items.menu_id')->where('menus.slug', '=', 'main-menu')->where('menus.status', '=', 'active')->where('menu_items.status', '=', 'active')->orderBy('menu_order', 'asc');
			?>
			@if( $menu->count() > 0)
				<ul class="nav navbar-nav navbar-right">
					@foreach( $menu->get() as $menu_item )
						@if( $menu_item->url == 'login' )
							@if (Auth::check())
							<?php
								$user = getUserRecord();
								$url = URL_USERS_DASHBOARD;
								if( in_array($user->role_id, array( OWNER_ROLE_ID, ADMIN_ROLE_ID, EXECUTIVE_ROLE_ID )) ) {
									$url = URL_DASHBOARD;
								} elseif( $user->role_id == USER_ROLE_ID ) {
									$url = URL_USERS_DASHBOARD;
								} elseif( $user->role_id == VENDOR_ROLE_ID ) {
									$url = URL_VENDOR_DASHBOARD;
								}
							?>
							
							<li {{ isActive($main_active, 'login') }}><a href="{{ $url }}">{{ getPhrase('My Dashboard') }}</a></li>
							
							<li>	
							<a href="{{URL_MESSAGES}}">
							<i class="fa fa-envelope-o"></i>
							<span class="badge badge-success"></span><sup>{{$count = Auth::user()->newThreadsCount()}}</sup>
							</a></li>
							@else
								<li {{ isActive($main_active, 'login') }}><a href="{{ URL_USERS_LOGIN }}">{{ getPhrase('LOGIN') }}</a></li>
							@endif	
						@else
						<?php						
						$url = PREFIX . $menu_item->url;
						if( $menu_item->page_id > '0' ) {
							$page = App\Pages::where('id', '=', $menu_item->page_id)->first();
							if( $page ) {
								$url = PREFIX . 'page/' .$page->slug;
							}
						}
						$target = '';
						if( $menu_item->target == '_blank' ) {
							$target = ' target="_blank"';
						}
						?>
						<li {{ isActive($main_active, $menu_item->menu_active_title) }}><a href="{{ $url }}" {{ $target }}>{{ $menu_item->title }}</a></li>
						@endif
					@endforeach
					<!--for cart-->
					<li>
						<a href="{{ URL_CART }}"><span class="fa fa-shopping-cart"></span> <sup>{{ sizeof(Cart::content()) }}</sup></a>

					</li>
				</ul>
				
			@else
			<ul class="nav navbar-nav navbar-right">
				
				<!--for home-->
				<li {{ isActive($main_active, 'home') }}><a href="{{ PREFIX }}">{{ getPhrase('HOME') }}</a></li>
				<!--for products-->
				<?php
				$categories = App\Category::where('status', '=', 'Active');
				?>
				@if( $categories->count() > 0 )
				<li class="dropdown {{ isActive($main_active, 'products') }}">
					<a href="{{ URL_DISPLAY_PRODUCTS }}">{{ getPhrase('PRODUCTS') }}</a>
					<ul class="dropdown-menu multi-level" role="menu">
						@foreach( $categories->get() as $category)
						<li class="dropdown-submenu">
							@if( $category->parent_id == 0 )
							<a href="{{ URL_DISPLAY_PRODUCTS . '/' . $category->slug }}"><span class="fa fa-product-hunt"></span>{{ $category->title }}<span class="fa fa-chevron-right"></span></a>							
							<?php
							$subcats = App\Category::where('status', '=', 'Active')->where('parent_id', '=', $category->id);
							?>
							@if( $subcats->count() > 0 )
							<ul class="dropdown-menu">
								@foreach( $subcats->get() as $subcat)
									<li><a href="{{ URL_DISPLAY_PRODUCTS . '/' . $category->slug . '/' . $subcat->slug }}"> {{ $subcat->title }}</a> </li>
								@endforeach
							</ul>
							@endif
							@endif
						</li>
						@endforeach	
					</ul>
				</li>
				@endif

				<li class="{{ isActive($main_active, 'about') }}"><a href="about.html">ABOUT</a></li>
				<li class="{{ isActive($main_active, 'faq') }}"><a href="faq.html">FAQ</a></li>
				<li class="{{ isActive($main_active, 'support') }}"><a href="support.html">SUPPORT</a></li>
				<li class="{{ isActive($main_active, 'contact') }}"><a href="contact.html">CONTACT</a></li>
				@if (Auth::check())
				<?php
					$user = getUserRecord();
					$url = URL_USERS_DASHBOARD;
					if( in_array($user->role_id, array( OWNER_ROLE_ID, ADMIN_ROLE_ID, EXECUTIVE_ROLE_ID )) ) {
						$url = URL_DASHBOARD;
					} elseif( $user->role_id == USER_ROLE_ID ) {
						$url = URL_USERS_DASHBOARD;
					} elseif( $user->role_id == VENDOR_ROLE_ID ) {
						$url = URL_VENDOR_DASHBOARD;
					}
				?>
				
				<li {{ isActive($main_active, 'login') }}><a href="{{ $url }}">{{ getPhrase('My Dashboard') }}</a></li>
                
                <li>	
				<a href="{{URL_MESSAGES}}">
              <i class="fa fa-envelope-o"></i>
              <span class="badge badge-success"></span><sup>{{$count = Auth::user()->newThreadsCount()}}</sup>
            </a></li>

				@else
					<li {{ isActive($main_active, 'login') }}><a href="{{ URL_USERS_LOGIN }}">{{ getPhrase('LOGIN') }}</a></li>
				@endif
				
				<li>
            
            
          </li>

				<!--for cart-->
				<li>
					<a href="{{ URL_CART }}"><span class="fa fa-shopping-cart"></span> <sup>{{ sizeof(Cart::content()) }}</sup></a>

				</li>
			</ul>
			@endif

		</div>
	</div>
</nav>
<!--/HEADER-->