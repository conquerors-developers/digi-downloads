<ul class="nav nav-tabs">
	<li class="<?php if($tab == 'dashboard') echo 'active';?>"><a href="{{ URL_USERS_DASHBOARD }}">Dashboard</a></li>
	<li class="<?php if($tab == 'purchases') echo 'active';?>"><a href="{{ URL_USERS_DASHBOARD . '/purchases' }}">Purchase History</a></li>
	<li class="<?php if($tab == 'setting') echo 'active';?>"><a href="{{ URL_USERS_DASHBOARD . '/setting' }}">Settings</a></li>
	
	<li><a href="{{URL_LOGOUT}}" onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();">Logout</a>
				   <form id="logout-form" action="{{ URL_LOGOUT }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
				   </li>
</ul>