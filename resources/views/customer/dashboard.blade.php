@extends($layout)
@section('header_scripts')
<link rel="stylesheet" href="{{CSS}}select2.css">
@stop

@section('content')
    <!--Inner Banner-->
    <section class="login-banner">
        <div class="container">
            <div class="row">
                <div class="div col-sm-12">
                    <h2>{{ Auth::user()->name }}</h2>
                </div>
            </div>
        </div>
    </section>
    <!--/Inner Banner-->
	
	<!--SECTION cart DASHBOARD-2-->
    <section class="dashboard2">
        <div class="container">
            <h2>{{ getPhrase('my_dashboard') }}</h2>
           
            @include('customer.menu', array('sub_active' => $sub_active, 'tab' => $tab))

            <div class="tab-content  animated fadeInDown">
                <div id="dashboard" class="tab-pane fade <?php if( $tab == 'dashboard' ) echo 'in active';?>">
                    
						<div><a>purchases</a></div>
						<div><a>Edit</a></div>
						<div><a>Profile</a></div>
					
                </div>
				
				<div id="history" class="tab-pane fade <?php if( $tab == 'purchases' ) echo 'in active';?>">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Product Name</th>
                                <th>Date</th>
                                <th>Amount</th>
                                <th>License</th>
                                <th>Download</th>
                            </tr>
                        </thead>
                        <tbody>
                            @include('customer.purchases', array('purchases' => $purchases, 'tab' => $tab))							
                        </tbody>
                    </table>
                </div>

                <div id="setting" class="tab-pane fade tab-setting <?php if( $tab == 'setting' ) echo 'in active';?>">
                    <?php
					$user = getUserRecord();
					?>
					@include('errors.errors')				
					{{ Form::model($user, array('url' => URL_USERS_DASHBOARD.'/'.$tab, 		'method'=>'post','name'=>'formName', 'files'=>'true' )) }}
					<div class="row">
                        <div class="col-md-6 col-sm-12">
                            <h4>{{ getPhrase('Edit Account Information') }}</h4>                          
                                <div class="form-group">
                                    {{ Form::label('first_name', getPhrase( 'First Name' ) ) }}
									
									{{ Form::text('first_name', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase( 'First Name' ), 
									'title' => getPhrase('First Name' ),
									'ng-model'=>'first_name',
									'required'=> 'true',
									'ng-class'=>'{"has-error": formName.first_name.$touched && formName.first_name.$invalid}',
									)) }}
									<div class="validation-error" ng-messages="formName.first_name.$error" >
										{!! getValidationMessage()!!}
									</div>
                                </div>
                                <div class="form-group">
									{{ Form::label('last_name', getPhrase( 'Last Name' ) ) }}
									{{ Form::text('last_name', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase( 'Last Name' ), 
									'title' => getPhrase('Last Name' ),
									'ng-model'=>'last_name',
									'required'=> 'true',
									'ng-class'=>'{"has-error": formName.last_name.$touched && formName.last_name.$invalid}',
									)) }}                                
									<div class="validation-error" ng-messages="formName.last_name.$error" >
										{!! getValidationMessage()!!}
									</div>
                                </div>
								
								<div class="form-group">
									{{ Form::label('image', getPhrase( 'Profile Image' ) ) }}
									{!! Form::file('image', array('id'=>'image_input', 'accept'=>'.png,.jpg,.jpeg')) !!}
									<?php if(isset($user) && $user) {
										  if($user->image!='') {
										?>
									<img src="{{ getProfilePath($user->image) }}" />
									<?php } } ?>
                                </div>
								
								
								
								
								
                                <div class="form-group">
{{ Form::label('email', getPhrase( 'Email address' ) ) }}
                                    {{ Form::text('email', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase( 'Email address' ), 
									'title' => getPhrase('Email address' ),
									'ng-model'=>'email',
									'required'=> 'true',
									'disabled' => true,
									'ng-class'=>'{"has-error": formName.email.$touched && formName.email.$invalid}',
									)) }}
                                </div>
                                
                                <div class="form-group">
{{ Form::label('password', getPhrase( 'Password' ) ) }}
                                    <input type="password" class="form-control" name="password" id="password" placeholder="{{ getPhrase( 'Password' ) }}">

                                </div>
                                <div class="form-group">
{{ Form::label('confirm_password', getPhrase( 'Re-enter Password' ) ) }}
                                    <input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="{{ getPhrase( 'Re-enter Password' ) }}">

                                </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <h4>{{ getPhrase('Edit Billing Address') }}</h4>                           

                                <div class="form-group">
{{ Form::label('billing_address1', getPhrase( 'Address Line1' ) ) }}
                                    {{ Form::text('billing_address1', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase( 'Address Line1' ), 
									'title' => getPhrase('Address Line1' ),
									'ng-model'=>'billing_address1',
									'ng-class'=>'{"has-error": formName.billing_address1.$touched && formName.billing_address1.$invalid}',
									)) }}									
                                </div>
                                <div class="form-group">
{{ Form::label('billing_address2', getPhrase( 'Address Line2' ) ) }}
                                    {{ Form::text('billing_address2', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase( 'Address Line2' ), 
									'title' => getPhrase('Address Line2' ),
									'ng-model'=>'billing_address2',
									'ng-class'=>'{"has-error": formName.billing_address2.$touched && formName.billing_address2.$invalid}',
									)) }}									
                                </div>
                                <div class="form-group">
{{ Form::label('first_name', getPhrase( 'City' ) ) }}
                                    {{ Form::text('billing_city', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase( 'City' ), 
									'title' => getPhrase('City' ),
									'ng-model'=>'billing_city',
									'ng-class'=>'{"has-error": formName.billing_city.$touched && formName.billing_city.$invalid}',
									)) }}
                                </div>
                                <div class="form-group">
{{ Form::label('first_name', getPhrase( 'Zip Code' ) ) }}
                                    {{ Form::text('billing_zip', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase( 'Zip Code' ), 
									'title' => getPhrase('Zip Code' ),
									'ng-model'=>'billing_zip',
									'ng-class'=>'{"has-error": formName.billing_zip.$touched && formName.billing_zip.$invalid}',
									)) }}
                                </div>
                                <div class="form-group">
{{ Form::label('first_name', getPhrase( 'State/Province' ) ) }}
									{{ Form::text('billing_state', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase( 'State/Province' ), 
									'title' => getPhrase('State/Province' ),
									'ng-model'=>'billing_state',
									'ng-class'=>'{"has-error": formName.billing_state.$touched && formName.billing_state.$invalid}',
									)) }}

                                </div>
                                <div class="form-group">
{{ Form::label('first_name', getPhrase( 'Country' ) ) }}
                                    <?php
									$countries = array_pluck( App\Countries::where('status', '=', 'Active')->get(), 'name', 'name' );
									?>
									{{Form::select('billing_country', $countries, null, ['class'=>'form-control select2', "id"=>"billing_country"])}}									
                                </div>

                            

                        </div>
                        <div class="save">
                            <button type="submit" class="btn btn-primary">SAVE CHANGES</button>
                        </div>
                    </div>
					</form>
                </div>

                <div id="key" class="tab-pane fade">
                    <h4></h4>
                </div>
                <div id="logout" class="tab-pane fade">
                    <h4></h4>

                </div>
            </div>
        </div>
    </section>
    <!--/SECTION cart checkout-->
@endsection

@section('footer_scripts')	
	@include('common.validations')
	@include('common.alertify')

	@include('common.select2')
	<script>
	var file = document.getElementById('image_input');
	file.onchange = function(e){
	var ext = this.value.match(/\.([^\.]+)$/)[1];
	switch(ext)
	{
	case 'jpg':
	case 'jpeg':
	case 'png':
	break;
	default:
	alertify.error("{{getPhrase('file_type_not_allowed')}}");
	this.value='';
	}
	};
	</script>
@stop