@if( $purchases->count() > 0 )
	@foreach( $purchases as $purchase )
	<tr>
		<td class="col1">#8325</td>
		<td class="col2">{{ $purchase->item_name}}</td>
		<td class="col3">{{ $purchase->created_at }}</td>
		<td class="col4">{{ currency( $purchase->paid_amount ) }}</td>
		<td class="col5">Standard</td>
		<td class="col6">
		<a href="{{ URL_CART_DOWNLOAD . $purchase->slug}}">
		<span class="fa fa-download"></span>
		</a>
		</td>
	</tr>
	@endforeach
	{{ $purchases->links() }}
	@else
		<tr> <td colspan="6" align="center"> {{ getPhrase('No Purchases found found') }} 
	<?php echo sprintf( getPhrase('Click %s to purchase'), '<a href="'.URL_DISPLAY_PRODUCTS.'">'.getPhrase('here').'</a>' );?> </td></tr>
@endif