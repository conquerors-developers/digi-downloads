@extends('layouts.layout-site')

@section('content')

    <!--Inner banner-->
    <section class="login-banner">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h3>UI Kits, Themes, HTML Templates Starting From $5</h3>
                    <p class="loginbanner-data">The European languages are members of the same family. Their separate.</p>
                </div>
            </div>
        </div>
    </section>
    <!--/Inner banner-->

    <!--section-4 CATEGORIES-->
    <div class="categories ">
        <div class="container">
            <div class="row cs-row">
                <div class="col-md-9 col-sm-12">
                    <!--for right side columns-->
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <!-- Product -->
                            <div class="product">
                                <div class="portfolio-item">
                                    <img src="images/1.png" class="img-responsive" alt="">
                                    <!-- portfolio item hover -->
                                    <div class="portfolio-hover">
                                        <div class="portfolio-hover-content font-reg">
                                            <ul class="pair-btns">
                                                <li><a href="#" class="btn btn-buy">Buy Now</a></li>
                                                <li><a href="#" class="btn btn-view">Details</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-content">
                                    <span class="product-price">15$</span>
                                    <a href="#" class="product-title"> Amples- Creative Agency PSD Template </a>
                                    <a href="#">
                                        <div class="product-author ">
                                            <div class="media-left">
                                                <img src="images/author.png" alt="user-avatar" class=" img-circle">
                                            </div>
                                            <div class="media-body">
                                                <p>Yaswin Satya </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <!-- /Product -->
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <!-- Product -->
                            <div class="product">
                                <div class="portfolio-item">
                                    <img src="images/6.png" class="img-responsive" alt="">
                                    <!-- portfolio item hover -->
                                    <div class="portfolio-hover">
                                        <div class="portfolio-hover-content font-reg">
                                            <ul class="pair-btns">
                                                <li><a href="#" class="btn btn-buy">Buy Now</a></li>
                                                <li><a href="#" class="btn btn-view">Details</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-content">
                                    <span class="product-price">15$</span>
                                    <a href="#" class="product-title"> Amples- Creative Agency PSD Template </a>
                                    <a href="#">
                                        <div class="product-author ">
                                            <div class="media-left">
                                                <img src="images/author.png" alt="user-avatar" class=" img-circle">
                                            </div>
                                            <div class="media-body">
                                                <p>Yaswin Satya </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <!-- /Product -->
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <!-- Product -->
                            <div class="product">
                                <div class="portfolio-item">
                                    <img src="images/3.png" class="img-responsive" alt="">
                                    <!-- portfolio item hover -->
                                    <div class="portfolio-hover">
                                        <div class="portfolio-hover-content font-reg">
                                            <ul class="pair-btns">
                                                <li><a href="#" class="btn btn-buy">Buy Now</a></li>
                                                <li><a href="#" class="btn btn-view">Details</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-content">
                                    <span class="product-price">15$</span>
                                    <a href="#" class="product-title"> Amples- Creative Agency PSD Template </a>
                                    <a href="#">
                                        <div class="product-author ">
                                            <div class="media-left">
                                                <img src="images/author.png" alt="user-avatar" class=" img-circle">
                                            </div>
                                            <div class="media-body">
                                                <p>Yaswin Satya </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <!-- /Product -->
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <!-- Product -->
                            <div class="product">
                                <div class="portfolio-item">
                                    <img src="images/6.png" class="img-responsive" alt="">
                                    <!-- portfolio item hover -->
                                    <div class="portfolio-hover">
                                        <div class="portfolio-hover-content font-reg">
                                            <ul class="pair-btns">
                                                <li><a href="#" class="btn btn-buy">Buy Now</a></li>
                                                <li><a href="#" class="btn btn-view">Details</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-content">
                                    <span class="product-price">15$</span>
                                    <a href="#" class="product-title"> Amples- Creative Agency PSD Template </a>
                                    <a href="#">
                                        <div class="product-author ">
                                            <div class="media-left">
                                                <img src="images/author.png" alt="user-avatar" class=" img-circle">
                                            </div>
                                            <div class="media-body">
                                                <p>Yaswin Satya </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <!-- /Product -->
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <!-- Product -->
                            <div class="product">
                                <div class="portfolio-item">
                                    <img src="images/4.png" class="img-responsive" alt="">
                                    <!-- portfolio item hover -->
                                    <div class="portfolio-hover">
                                        <div class="portfolio-hover-content font-reg">
                                            <ul class="pair-btns">
                                                <li><a href="#" class="btn btn-buy">Buy Now</a></li>
                                                <li><a href="#" class="btn btn-view">Details</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-content">
                                    <span class="product-price">15$</span>
                                    <a href="#" class="product-title"> Amples- Creative Agency PSD Template </a>
                                    <a href="#">
                                        <div class="product-author ">
                                            <div class="media-left">
                                                <img src="images/author.png" alt="user-avatar" class=" img-circle">
                                            </div>
                                            <div class="media-body">
                                                <p>Yaswin Satya </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <!-- /Product -->
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <!-- Product -->
                            <div class="product">
                                <div class="portfolio-item">
                                    <img src="images/6.png" class="img-responsive" alt="">
                                    <!-- portfolio item hover -->
                                    <div class="portfolio-hover">
                                        <div class="portfolio-hover-content font-reg">
                                            <ul class="pair-btns">
                                                <li><a href="#" class="btn btn-buy">Buy Now</a></li>
                                                <li><a href="#" class="btn btn-view">Details</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-content">
                                    <span class="product-price">15$</span>
                                    <a href="#" class="product-title"> Amples- Creative Agency PSD Template </a>
                                    <a href="#">
                                        <div class="product-author ">
                                            <div class="media-left">
                                                <img src="images/author.png" alt="user-avatar" class=" img-circle">
                                            </div>
                                            <div class="media-body">
                                                <p>Yaswin Satya </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <!-- /Product -->
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <!-- Product -->
                            <div class="product">
                                <div class="portfolio-item">
                                    <img src="images/5.png" class="img-responsive" alt="">
                                    <!-- portfolio item hover -->
                                    <div class="portfolio-hover">
                                        <div class="portfolio-hover-content font-reg">
                                            <ul class="pair-btns">
                                                <li><a href="#" class="btn btn-buy">Buy Now</a></li>
                                                <li><a href="#" class="btn btn-view">Details</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-content">
                                    <span class="product-price">15$</span>
                                    <a href="#" class="product-title"> Amples- Creative Agency PSD Template </a>
                                    <a href="#">
                                        <div class="product-author ">
                                            <div class="media-left">
                                                <img src="images/author.png" alt="user-avatar" class=" img-circle">
                                            </div>
                                            <div class="media-body">
                                                <p>Yaswin Satya </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <!-- /Product -->
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <!-- Product -->
                            <div class="product">
                                <div class="portfolio-item">
                                    <img src="images/4.png" class="img-responsive" alt="">
                                    <!-- portfolio item hover -->
                                    <div class="portfolio-hover">
                                        <div class="portfolio-hover-content font-reg">
                                            <ul class="pair-btns">
                                                <li><a href="#" class="btn btn-buy">Buy Now</a></li>
                                                <li><a href="#" class="btn btn-view">Details</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-content">
                                    <span class="product-price">15$</span>
                                    <a href="#" class="product-title"> Amples- Creative Agency PSD Template </a>
                                    <a href="#">
                                        <div class="product-author ">
                                            <div class="media-left">
                                                <img src="images/author.png" alt="user-avatar" class=" img-circle">
                                            </div>
                                            <div class="media-body">
                                                <p>Yaswin Satya </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <!-- /Product -->
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <!-- Product -->
                            <div class="product">
                                <div class="portfolio-item">
                                    <img src="images/6.png" class="img-responsive" alt="">
                                    <!-- portfolio item hover -->
                                    <div class="portfolio-hover">
                                        <div class="portfolio-hover-content font-reg">
                                            <ul class="pair-btns">
                                                <li><a href="#" class="btn btn-buy">Buy Now</a></li>
                                                <li><a href="#" class="btn btn-view">Details</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-content">
                                    <span class="product-price">15$</span>
                                    <a href="#" class="product-title"> Amples- Creative Agency PSD Template </a>
                                    <a href="#">
                                        <div class="product-author ">
                                            <div class="media-left">
                                                <img src="images/author.png" alt="user-avatar" class=" img-circle">
                                            </div>
                                            <div class="media-body">
                                                <p>Yaswin Satya </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <!-- /Product -->
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <!-- Product -->
                            <div class="product">
                                <div class="portfolio-item">
                                    <img src="images/7.png" class="img-responsive" alt="">
                                    <!-- portfolio item hover -->
                                    <div class="portfolio-hover">
                                        <div class="portfolio-hover-content font-reg">
                                            <ul class="pair-btns">
                                                <li><a href="#" class="btn btn-buy">Buy Now</a></li>
                                                <li><a href="#" class="btn btn-view">Details</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-content">
                                    <span class="product-price">15$</span>
                                    <a href="#" class="product-title"> Amples- Creative Agency PSD Template </a>
                                    <a href="#">
                                        <div class="product-author ">
                                            <div class="media-left">
                                                <img src="images/author.png" alt="user-avatar" class=" img-circle">
                                            </div>
                                            <div class="media-body">
                                                <p>Yaswin Satya </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <!-- /Product -->
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <!-- Product -->
                            <div class="product">
                                <div class="portfolio-item">
                                    <img src="images/7.png" class="img-responsive" alt="">
                                    <!-- portfolio item hover -->
                                    <div class="portfolio-hover">
                                        <div class="portfolio-hover-content font-reg">
                                            <ul class="pair-btns">
                                                <li><a href="#" class="btn btn-buy">Buy Now</a></li>
                                                <li><a href="#" class="btn btn-view">Details</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-content">
                                    <span class="product-price">15$</span>
                                    <a href="#" class="product-title"> Amples- Creative Agency PSD Template </a>
                                    <a href="#">
                                        <div class="product-author ">
                                            <div class="media-left">
                                                <img src="images/author.png" alt="user-avatar" class=" img-circle">
                                            </div>
                                            <div class="media-body">
                                                <p>Yaswin Satya </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <!-- /Product -->
                        </div>

                    </div>

                    <!--for pagination-->
                    <ul class="pagination">

                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><span> . </span></li>
                        <li><span> . </span></li>
                        <li><span> . </span></li>
                        <li><span> . </span></li>
                        <li><span> . </span></li>
                        <li><span> . </span></li>


                        <li><a href="#">14</a></li>

                    </ul>
                </div>
                <div class="col-md-3 col-sm-12  animated fadeInDown">
                    <!--for right side-->
                    <div class="author-details">
                        <img src="images/author.png" alt="">
                        <h3>Mark Mathew</h3>
                        <h6>markmathew1234@gmail.com</h6>
                        <div class="author-icons">
                            <ul>
                                <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                                <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                <li><a href="#"><span class="fa fa-pinterest-p"></span></a></li>
                                <li><a href="#"><span class="fa fa-dribbble"></span></a></li>
                            </ul>
                        </div>
                        <h6>We work with clients big and small all over the world and we utilize all forms</h6>
                    </div>
                    <div class="author-form">
                        <h2>FILL DETAILS HERE</h2>
                        <form>
                            <div class="form-group">
                                <label>First Name<sup>*</sup></label>
                                <input type="text" class="form-control">

                            </div>
                            <div class="form-group">
                                <label>Last Name<sup>*</sup></label>
                                <input type="text" class="form-control">

                            </div>
                            <div class="form-group">
                                <label>Email<sup>*</sup></label>
                                <input type="email" class="form-control">

                            </div>
                            <div class="form-group">
                                <label>Subject<sup>*</sup></label>
                                <input type="text" class="form-control">

                            </div>
                            <div class="form-group">
                                <label>Message<sup>*</sup></label>
                                <textarea class="form-control" rows="3"></textarea>
                            </div>
                            <div class="author-place">
                                <button type="button" class="btn btn-default">SEND MESSAGE</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/section-4 CATEGORIES-->

    <!--SECTION-5 SIGN UP-->
    <section class="signup">
        <div class="container">
            <h3>Nullam quis ante.Etiam sit amet orci eget eros faucibus tincidunt</h3>
            <div class="sign">
                <div class="input-group">
                    <input type="email" class="form-control" placeholder="Email Address">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">SIGN UP</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/SECTION-5 SIGN UP-->
@endsection