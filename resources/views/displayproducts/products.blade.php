<?php
$extra_class = '';
if( isset($type) && $type == 'related')
	$extra_class = 'product-lg';
?>
@if( $products->count() > 0 )
	@foreach( $products as $product )
	<div class="col-md-4 col-sm-6">
		<!-- Product -->
		<div class="product {{ $extra_class }}">
			<div class="portfolio-item">
				<?php
				$product_image = DEFAULT_PRODUCT_IMAGE_THUMBNAIL;
				if( $product->image != '' && File::exists(UPLOAD_PATH_PRODUCTS_THUMBNAIL . $product->image ) ) {
					$product_image = UPLOAD_URL_PRODUCTS_THUMBNAIL . $product->image;
				}
				?>
				<img src="{{ $product_image }}" class="img-responsive" alt="">
				<!-- portfolio item hover -->
				<div class="portfolio-hover">
					<div class="portfolio-hover-content font-reg">
						<ul class="pair-btns">
							@if($product->price_type == 'default')
							<li>
							<form action="{{ URL_DISPLAY_PRODUCTS_CART }}" method="POST" class="side-by-side">
								{!! csrf_field() !!}
								@if( $product->price_type == 'default' )
								<input type="hidden" name="id" value="{{ $product->id }}">
								<input type="hidden" name="name" value="{{ $product->name }}">
								<input type="hidden" name="price" value="{{ $product->price }}">
								@endif
								<input type="hidden" name="price_type" value="{{ $product->price_type }}">
								<button type="submit" class="btn btn-buy">{{ getPhrase('BUY NOW') }}</button>
							</form>
							</li>
							@endif
							<li><a href="{{ URL_DISPLAY_PRODUCTS_DETAILS . $product->slug }}" class="btn btn-view">{{ getPhrase('Details') }}</a></li>
						</ul>
					</div>
				</div>
			</div>
			<?php
			$author = getUserRecord( $product->user_created);
			$author_image = DEFAULT_USERS_IMAGE_THUMBNAIL;
			$author_name = $author->name;
			if( File::exists(IMAGE_PATH_USERS_THUMBNAIL . $author->image ) ) {
				$author_image = IMAGE_PATH_USERS_THUMBNAIL . $author->image;
			}
			?>
			<div class="product-content">
				<span class="product-price">
				@if( $product->price_type == 'default' )
					{{ currency( $product->price ) }}
				@else
				<?php
					$price_variations = json_decode( $product->price_variations );
					$min_price = $max_price = $index = 0;
					if( ! empty( $price_variations ) ) {
						foreach( $price_variations as $key => $item ) {
							if( $index == 0)
								$min_price = $item->amount;
							if( $item->amount < $min_price )
								$min_price = $item->amount;
							if( $item->amount > $max_price )
								$max_price = $item->amount;
							$index++;
						}
					}
				?>
				{{ currency( $min_price ) }} - {{ currency( $max_price ) }}
				@endif
				</span>
				<a href="{{ URL_DISPLAY_PRODUCTS_DETAILS . $product->slug }}" class="product-title"> {{ $product->name }} </a>
				<a href="#">
					<div class="product-author ">
						<div class="media-left">
							<img src="{{ $author_image }}" alt="user-avatar" class=" img-circle">
						</div>
						<div class="media-body">
							<p>{{ $author_name }}</p>
						</div>
					</div>
				</a>
			</div>
		</div>
		<!-- /Product -->
	</div>
	@endforeach
	@if( ! isset($nopagelinks) )
		{{ $products->links() }}
	@endif
	@else
		<div class="col-md-4 col-sm-6"> {{ getPhrase('No Products found') }}
		</div>
	@endif
