@extends('layouts.layout-site')

@section('content')
<!--SECTION-1 BANNER-->
<section class="login-banner">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h3>{{ $product->name }}</h3>
				<ol class="breadcrumb">
					<li><a href="{{ PREFIX }}">{{ getPhrase('Home') }}</a></li>
					<li><a href="{{ URL_DISPLAY_PRODUCTS }}">{{ getPhrase('Products') }}</a></li>
					<li class="active">{{ $product->name }}</li>
				</ol>
			</div>
		</div>
	</div>
</section>
<!--/SECTION BANNER-->

<?php
$user = getUserRecord($product->user_created);
?>
<!--SECTION THEMES-->
<section class="theme">
	<div class="container">
		<div class="row">
			
			<div class="col-md-9 col-sm-12">
				@include('errors.errors') 
				<!--for 1st container -left side section-->
				<div class="kingma">
					<?php
					$product_image = DEFAULT_PRODUCT_IMAGE;
					$price = $product->price;
					
					if( $product->image != '' && File::exists(UPLOAD_PATH_PRODUCTS . $product->image ) ) {
						$product_image = UPLOAD_URL_PRODUCTS . $product->image;
					}
					?>
					<img src="{{ $product_image }}" alt="" class="img-responsive">
					<form action="{{ URL_DISPLAY_PRODUCTS_CART }}" method="POST" class="side-by-side" name="frmItem">
						{!! csrf_field() !!}
						<div class="kingma-buttons">
							@if($product->demo_link != '')
								<a class="btn btn-primary" href="{{ $product->demo_link}}" target="_blank">{{ getPhrase('LIVE DEMO') }}</a>&nbsp;
							@endif
							<input type="hidden" name="id" value="{{ $product->id }}">
							<input type="hidden" name="name" value="{{ $product->name }}">
							@if( $product->price_type == 'default' )							
								<input type="hidden" name="price" class="price" value="{{ $product->price }}">
								<input type="hidden" name="price_class" class="price_class" value="{{ $product->price }}">
							@else
								<?php
								$price_variations = json_decode( $product->price_variations );
								if( ! empty( $price_variations ) ) {
									foreach( $price_variations as $key => $item ) {
										$checked = '';
										if( isset( $item->isdefault ) && $item->isdefault == 'on' )
											$checked = ' checked';
										if( $product->price_display_type == 'radio' ) {
											echo '<input type="radio" name="price[]" value="'.$item->index.'" '.$checked.'>&nbsp;' . $item->name . ' ' . currency( $item->amount ) . '<br>';
											echo '<input type="hidden" name="price_class" class="price_class" value="'.$item->amount.'">';
										} else {
										echo '<input type="checkbox" name="price[]" value="'.$item->index.'" '.$checked.'>&nbsp;' . $item->name . ' ' . currency( $item->amount ) . '<br>';
										}
									}
								}
								?>
								
							@endif
						</div>
						<input type="hidden" name="price_type" value="{{ $product->price_type }}">
						<input type="hidden" name="licence" id="licence" value="0">
						<button type="submit" class="btn btn-default">{{ getPhrase('BUY NOW') }} <span class="price_lable">@if( $product->price_type == 'default' ) {{ currency( $product->price) }} @endif</span></button>
					</form>
				</div>
				
				<div class="share-icons">
					<?php
					$product_url = URL_DISPLAY_PRODUCTS_DETAILS . $product->slug;
					$product_title = $product->name;
					?>
					<ul>
						<li><a href="#">{{ getPhrase('Share:') }}</a></li>						
						<li><a href="{{ Share::load($product_url, $product_title)->facebook() }}"><span class="fa fa-facebook"></span></a></li>
						<li><a href="{{ Share::load($product_url, $product_title)->twitter() }}"><span class="fa fa-twitter"></span></a></li>
						<li><a href="{{ Share::load($product_url, $product_title)->gplus() }}"><span class="fa fa-google-plus"></span></a></li>
						<li><a href="{{ Share::load($product_url, $product_title)->pinterest() }}"><span class="fa fa-pinterest"></span></a></li>
						<li><a href="{{ Share::load($product_url, $product_title)->digg() }}"><span class="fa fa-dribbble"></span></a></li>

					</ul>
				</div>
				<!--for 2nd container -left side-->
				<div class="kingma-details">
					<h6 class="widget-title">{!! $product->name !!} {{ getPhrase('DETAILS') }}</h6>
					<p class="kingma-para">{!! $product->description !!}</p>
				</div>

			</div>

			<div class="col-md-3 col-sm-12">
				<!--for right side section-->
				<div class="add-cart">


					@if($product->licences != '')
					<div class="input-group ">
						<div class="radio">
							<div>
								<label>
									<input type="radio" checked="" value="0" name="licence_radio" onclick="addLicence(0, 0)">
									<span class="radio-content">
										<span class="item-content">{{ getPhrase('Regular Licence') }}</span>
									<i aria-hidden="true" class="fa uncheck fa-circle-thin"></i>
									<i aria-hidden="true" class="fa check fa-dot-circle-o"></i>
									</span>
								</label>
							</div>
							<?php
							$licences = (array) json_decode( $product->licences );
							if( ! empty( $licences ) )
							{
								foreach( $licences as $licence) {
									$details = App\Licence::where('id', '=', $licence)->first();
							?>
							<div>
								<label>
									<input type="radio" value="{{ $details->id }}" name="licence" onclick="addLicence(<?php echo $details->price;?>, {{ $details->id }})">
									<span class="radio-content">
										<span class="item-content">{{ $details->title . '( '.currency( $details->price ) .' )' }}</span>
									<i aria-hidden="true" class="fa uncheck fa-circle-thin"></i>
									<i aria-hidden="true" class="fa check fa-dot-circle-o"></i>
									</span>
								</label>
							</div>
								<?php } ?>							
							<?php } ?>
						</div>
					</div>


					<h1 class="price_lable">{{ currency( $price ) }}</h1>

					<form>
						<div class="addcart-buttons">
							<!--<button type="button" class="btn btn-primary">ADD TO CART</button> &nbsp;-->
							<button type="button" class="btn btn-default" onclick="javascript:document.frmItem.submit();">{{ getPhrase('BUY', 'upper') }} <span class="price_lable">{{ currency( $price ) }}</span></button>
						</div>
					</form>
				</div>
				@endif

				
				<div class="author ">
					<h5 class="widget-title-center">{{ getPhrase( 'ABOUT AUTHOR', 'upper' ) }}</h5>
					<img src="images/author.png" alt="">
					<h6>{{ $user->name }}</h6>
					<p class="auth-para">{{ getPhrase('products : ') }} {{ App\Product::where('user_created', '=', $user->id)->where('status', '=', 'Active')->count() }}</p>
					<a class="btn btn-default" href="{{ URL_DISPLAYPRODUCTS_VENDORDETAILS . $user->slug }}">{{ getPhrase('view_details', 'upper') }}</a>
					
					<button type="button" class="btn btn-default">SEND MESSAGE</button>
				</div>
				@if( $product->licence_of_use != '')
				<div class="license">
					<h5>{{ getPhrase('LICENSE OF USE', 'upper') }}</h5>
					<hr>
					{!! $product->licence_of_use !!}
				</div>
				@endif
				<div class="product-info">
					<h5 class="widget-title">{{ getPhrase('PRODUCT INFO', 'upper') }}</h5>
					<ul>
						<li><a href="javascript:void(0);">{{ getPhrase('Date') }}<span >{{ displayDate( $product->created_at ) }}</span></a></li>
						<?php
						$payment = App\Payment::join('payments_items', 'payments.id', '=', 'payments_items.payment_id')->where('payments_items.item_id', '=', $product->id);
						?>
						<li><a href="javascript:void(0);">{{ getPhrase('Downloads') }}<span>{{ $payment->count() }}</span></a></li>
						
						<?php
						$rating = $payment->join('product_items_ratings', 'product_items_ratings.item_id', '=', 'payments_items.item_id')
						?>
						@if($rating->count() > 0)
						<li><a href="javascript:void(0);">{{ getPhrase('Ratings') }}<span>
						{{ round($rating->sum('rating') / $rating->count()) }} / 5
						</span></a></li>
						@endif
						<li><a href="#">Share<span>324</span></a></li>
						@if( $product->product_format != '')
						<li><a href="#">{{ getPhrase('Format') }}<span>{{ $product->product_format }}</span></a></li>
						@endif
					</ul>
					@if( $product->meta_tag_keywords != '')
					<div class="tags">
						<div class="tag-name">{{ getPhrase('Tags') }} :&nbsp;&nbsp;
						<?php $tags = explode(',', strip_tags( $product->meta_tag_keywords ) );?>
						@if( ! empty( $tags ) )
							@foreach( $tags as $tag )
							<span><a href="{{ URL_DISPLAY_PRODUCTS . '/tag:' . $tag}}">{{ $tag }}</a></span>,
							@endforeach
						@endif
						</div>
					</div>
					@endif

				</div>
				
				@if( $product->technical_info != '')
				<div class="technical-info">
					<h5 class="widget-title">{{ getPhrase('TECHNICAL INFO', 'upper') }}</h5>
						{!! $product->technical_info !!}					
				</div>
				@endif

			</div>
		</div>
		<!--SECTION THEME-->
	</div>
</section>

<!--section 3-PRODUCTS-->
<?php
$categories = json_decode( $product->categories );
if( $categories ) {
	$related_products = App\Product::getProducts( array( 'category' =>  $categories) )->paginate( 3 );
} else {
	$related_products = App\Product::getProducts( array( 'category' =>  'name:' . $product->name) )->paginate( 3 );
}
?>
@if($related_products->count() > 0 )
<div class="products">
	<div class="container">
		<div class="row cs-row">
			<div class="col-md-12">
				<h2 class="heading ">{{ getPhrase('RELATED PRODUCTS', 'upper') }}</h2>
			</div>
			@include('displayproducts.products', array('products' => $related_products, 'nopagelinks' => true, 'type' => 'related'))			
			@if( $related_products->count() > 3 )
			<div class="col-lg-12">
				<div class="products-button">
					<a href="{{ URL_DISPLAY_PRODUCTS }}" class="btn btn-primary">{{ getPhrase('SEE ALL PRODUCTS', 'upper') }}</a>
				</div>
			</div>
			@endif
		</div>
	</div>
</div>
<!--/section 3-PRODUCTS-->
@endif
<script type="text/javascript">
function addLicence( price, id )
{
	var item_price = $('.price_class').val();
	var currency = "{{ getSetting('currency_symbol', 'cart-settings') }}";
	var currency_position = "{{ getSetting('currency_position', 'cart-settings') }}";
	var final_price = Number( item_price ) + Number( price );
	if( currency_position == 'before' ) {
		final_price = currency + final_price;
	} else if( currency_position == 'beforewithspace' ) {
		final_price = currency + ' ' + final_price;
	} else if( currency_position == 'after' ) {
		final_price =  final_price + currency;
	} else if( currency_position == 'afterwithspace' ) {
		final_price = final_price + ' ' + currency;
	}
	$('#licence').val( id );
	$('.price_lable').html( final_price );
	
}
</script>
@endsection