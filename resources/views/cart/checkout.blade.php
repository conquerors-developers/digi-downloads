@extends('layouts.layout-site')
@section('header_scripts')
	<link rel="stylesheet" href="{{CSS}}select2.css">
@endsection

@section('content')
<!--SECTION-1 BANNER-->
<section class="login-banner">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h2>{{ getPhrase('CHECK OUT') }}</h2>
			</div>
		</div>
	</div>
</section>
<!--/SECTION BANNER-->

<!--SECTION cart checkout-->
<section class="checkout">
<div class="container">
	<div class="row">
	@if(Auth::check())
		<?php
		$user = getUserRecord();
		?>
		{!! Form::model($user, array('url' => URL_PAYNOW, 'method' => 'POST', 'name'=>'formName', 'id' => 'formName')) !!}
	@else
		{!! Form::open(array('url' => URL_PAYNOW, 'method' => 'POST', 'name'=>'formName', 'id' => 'formName')) !!}
	@endif
	
	@include('errors.errors')
	@if (session()->has('success_message'))
		<div class="alert alert-success">
			{{ session()->get('success_message') }}
		</div>
	@endif

	@if (session()->has('error_message'))
		<div class="alert alert-danger">
			{{ session()->get('error_message') }}
		</div>
	@endif
	
	<div class="col-md-8 col-sm-12">
		@if( Cart::total() > 0)
		<h2>{{ getPhrase('SELECT PAYMENT METHOD') }}</h2>		
		<div class="checkout-box">
			<div class="input-group ">
				<div class="radio">
				@foreach( $payment_gateways as $gateway )
				<div>
					<label>
						<input type="radio" value="{{ $gateway->slug }}" name="gateway">
						<span class="radio-content">
												<span class="item-content">{{ $gateway->title }}</span>
						<i aria-hidden="true" class="fa uncheck fa-circle-thin"></i>
						<i aria-hidden="true" class="fa check fa-dot-circle-o"></i>
						</span>
					</label>
				</div>
				@endforeach
				</div>
			</div>
		</div>
		@else
		<input type="hidden" name="gateway" value="Free">
		@endif

		<div class="check-form  animated fadeInDown">
		
		<div class="form-group">
		{{ Form::label('email', getPhrase( 'Email address' ) ) }} {!! required_field(); !!}
		{{ Form::text('email', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase( 'Email address' ), 
				'title' => getPhrase('Email address' ), 
				'data-toggle' => 'tooltip',
				'data-validation' => "[NOTEMPTY]"
				)) }}
	   </div>
	   <div class="form-group">
		{{ Form::label('first_name', getPhrase( 'First Name' ) ) }} {!! required_field(); !!}
		{{ Form::text('first_name', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase( 'First Name' ), 
				'title' => getPhrase('First Name' ), 
				'data-toggle' => 'tooltip',
				)) }}
	   </div>
		<div class="form-group">
		{{ Form::label('last_name', getPhrase( 'Last Name' ) ) }}
		{{ Form::text('last_name', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase( 'Last Name' ), 
				'title' => getPhrase('Last Name' ), 
				'data-toggle' => 'tooltip',
				)) }}
		</div>
		
		<h2>{{ getPhrase('Billinf Address') }}</h2>
		<div class="form-group">
		{{ Form::label('billing_address1', getPhrase( 'Address Line1' ) ) }}
			{{ Form::text('billing_address1', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase( 'Address Line1' ), 
			'title' => getPhrase('Address Line1' ),
			'ng-model'=>'billing_address1',
			'ng-class'=>'{"has-error": formName.billing_address1.$touched && formName.billing_address1.$invalid}',
			)) }}									
		</div>
		
		<div class="form-group">
		{{ Form::label('billing_address2', getPhrase( 'Address Line2' ) ) }}
			{{ Form::text('billing_address2', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase( 'Address Line2' ), 
			'title' => getPhrase('Address Line2' ),
			'ng-model'=>'billing_address2',
			'ng-class'=>'{"has-error": formName.billing_address2.$touched && formName.billing_address2.$invalid}',
			)) }}									
		</div>
		<div class="form-group">
		{{ Form::label('first_name', getPhrase( 'City' ) ) }}
			{{ Form::text('billing_city', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase( 'City' ), 
			'title' => getPhrase('City' ),
			'ng-model'=>'billing_city',
			'ng-class'=>'{"has-error": formName.billing_city.$touched && formName.billing_city.$invalid}',
			)) }}
		</div>
		<div class="form-group">
		{{ Form::label('first_name', getPhrase( 'Zip Code' ) ) }}
			{{ Form::text('billing_zip', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase( 'Zip Code' ), 
			'title' => getPhrase('Zip Code' ),
			'ng-model'=>'billing_zip',
			'ng-class'=>'{"has-error": formName.billing_zip.$touched && formName.billing_zip.$invalid}',
			)) }}
		</div>
		<div class="form-group">
		{{ Form::label('first_name', getPhrase( 'State/Province' ) ) }}
			{{ Form::text('billing_state', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase( 'State/Province' ), 
			'title' => getPhrase('State/Province' ),
			'ng-model'=>'billing_state',
			'ng-class'=>'{"has-error": formName.billing_state.$touched && formName.billing_state.$invalid}',
			)) }}

		</div>
		<div class="form-group">
		{{ Form::label('first_name', getPhrase( 'Country' ) ) }}
			<?php
			$countries = array_pluck( App\Countries::where('status', '=', 'Active')->get(), 'name', 'name' );
			?>
			{{Form::select('billing_country', $countries, null, ['class'=>'form-control select2', "id"=>"billing_country"])}}									
		</div>
		
		<button type="submit" class="btn btn-primary" >PURCHASE</button>
		
		</div>

	</div>
	{!! Form::close() !!}
	
	<div class="col-md-4 col-sm-12">
		  <h2>{{ getPhrase('TOTAL CART', 'upper') }}</h2>
		 <div class="total">
			<p class="fee">{{ getPhrase('PRODUCTS PRICE :', 'upper') }} <span>{{ currency( Cart::instance('default')->subtotal() ) }}</span></p>
			<p class="fee">{{ getPhrase('Tax :', 'upper') }} <span>{{ currency( Cart::instance('default')->tax() ) }}</span></p>
			<?php
			$licence_price = 0;
			?>
			@if(session()->has('licence_price'))
			<p class="fee">{{ getPhrase('SUPPORT FEE :', 'upper') }} <span>{{ currency( session()->get('licence_price') ) }}</span></p>
			<?php $licence_price = session()->get('licence_price');?>
			@endif
			
			<?php
			$discount_amount = 0;
			?>
			@if(session()->has('discount_amount'))
			<p class="fee">{{ getPhrase('COUPON CODE OFF :', 'upper') }} <span>{{ currency( session()->get('discount_amount') ) }}</span></p>
			<?php $discount_amount = session()->get('discount_amount');?>
			@endif
			
			<p class="fees">{{ getPhrase('TOTAL PRICE :', 'upper') }} <span>{{ currency( Cart::total() + $licence_price - $discount_amount ) }}</span></p>
		 </div>
		
		<div id="couponcode_message"></div>
		@if( ! session()->has('discount_amount') )
		<form action="{{ URL_CART_APPLYCOUPON }}" method="post" id="frmApplycoupon">			
			<div class="form-group">
				 <p class="coupon">{{ getPhrase('Have Coupon Code?') }}</p>
				<input type="text" name="code" id="code" class="form-control" placeholder="Enter Code Here">
			</div>		
			<div class="cart-button">
				<button type="submit" class="btn btn-primary">{{ getPhrase('APPLY') }}</button>
			</div>
		</form>
		@endif
		
		@if( session()->has('discount_amount') )
		<?php
		$coupon_details = session()->get('discount_details', '');
		?>
		@if( $coupon_details != '')
		@php
			$code = $coupon_details->code;
		@endphp
		<div class="coupon-apply">
			<p class="coupon-data"><span class="fa fa-check"></span>{{ getPhrase('Coupon Code') . $code . ' ' . currency( session()->get('discount_amount') ) . getPhrase(' applied') }}</p>
			<p class="coupon-data1">{{ currency( session()->get('discount_amount') ) . getPhrase(' reduced from the cart') }}</p>
			<p><a href="{{ URL_CART_REMOVECOUPON }}"><span class="fa fa-times"></span></a></p>
		</div>
		@endif
		@endif
		</div>
	</div>
</div>
</section>
<!--/SECTION cart checkout-->
 @endsection

@section('footer_scripts')
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js"></script>

	@include('common.select2')
	<script>

	$(document).ready(function () {
		$('#formName').validate({
			rules: {
				email: {
					required:true,
					email:true
				},
				first_name:'required'
			},
			messages:{
				'email' : "{{ getPhrase('Please enter email address') }}",
				'first_name' : "{{ getPhrase('Please enter first name') }}",
			}
		});
	});
	function validateCheckout()
	{
		if( ! document.getElementById('gateway') ) {
			swal({
				title : "{{getPhrase('info')}}",
				text : "{{getPhrase('Please select payment gateway')}}",
				type: "info",
			});
		} else {
			$( "#formName" ).submit();
		}
	}
	
	$('#frmApplycoupon').submit(function(e){
		e.preventDefault();
		
		var code = $('#code').val();
		if( code == '' ) {
			$('#couponcode_message').html('<div class="alert alert-danger">{{getPhrase("Please enter coupon code")}}</div>');
			return false;
		}
		
		
		$.ajax({
            url : '{{URL_CART_APPLYCOUPON}}',
			data : {
				coupon:$('#code').val(),
				_token:'{{ Session::token() }}'
			},
			method:'post',
            dataType: 'html',
        }).success(function (data) {
            var result = $.parseJSON(data);
			var message = '';
			if(!result.status) {
				message = '<div class="alert alert-danger">'+result.message+'</div>';
			} else {
				message = result.message;
				$('#frmApplycoupon').hide();
			}
			$('#couponcode_message').html( message );
        }).fail(function () {
            alert('Posts could not be loaded.');
        });
	});
    </script>
	@if( Cart::total() == 0 )
		<script type="text/javascript">
			document.formName.submit();
		</script>
	@endif
@endsection