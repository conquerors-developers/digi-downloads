@extends('layouts.layout-site')

@section('content')
<!--SECTION-1 BANNER-->
<section class="login-banner">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h2>{{ getPhrase('cart', 'upper') }}</h2>
			</div>
		</div>
	</div>
</section>
<!--/SECTION BANNER-->

<!--SECTION cart-->
<section class="cart  animated fadeInDown">
	<div class="container">
		@if (session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @endif

        @if (session()->has('error_message'))
            <div class="alert alert-danger">
                {{ session()->get('error_message') }}
            </div>
        @endif
		<div class="row">
			@if (sizeof(Cart::content()) > 0)
			<div class="col-md-8 col-sm-12">
				<h2>{{ getPhrase('YOUR CART') }}</h2>
				
				<table class="table">
					<thead>
						<tr>
							<th>{{ getPhrase('Image') }}</th>
							<th>{{ getPhrase('Product Name') }}</th>
							<th>{{ getPhrase('Product Price') }}</th>
							<th>{{ getPhrase('Options') }}</th>
						</tr>
					</thead>
					<tbody>
						@foreach (Cart::content() as $item)
						<tr>
							<td class="table-image">
							<?php
							$product_image = DEFAULT_PRODUCT_IMAGE_THUMBNAIL;
							if( $item->model->image != '' && File::exists(UPLOAD_PATH_PRODUCTS_THUMBNAIL . $item->model->image ) ) {
								$product_image = UPLOAD_URL_PRODUCTS_THUMBNAIL . $item->model->image;
							}
							?>
							<a href="{{ URL_DISPLAY_PRODUCTS_DETAILS . $item->model->slug }}"><img src="{{ $product_image }}" alt="product" class="img-responsive cart-image"></a></td>
							
							<td><a href="{{ URL_DISPLAY_PRODUCTS_DETAILS . $item->model->slug }}">{{ $item->name }}</a></td>
							
							<td class="colu2">{{ currency( $item->subtotal ) }}</td>
							<td class="colu3">
							<form action="{{ url('cart', [$item->rowId]) }}" method="POST" class="side-by-side">
                                {!! csrf_field() !!}
                                <input type="hidden" name="_method" value="DELETE">
                                <button type="submit" class="btn btn-danger btn-sm">
								<span class="fa fa-times"></span>
								</button>
                            </form>
							</td>

						</tr>
						@endforeach
						
					</tbody>
				</table>
								
			</div>
			<div class="col-md-4 col-sm-12">
				<h2>{{ getPhrase('TOTAL CART', 'upper') }}</h2>
				<div class="total">
					<p class="fee">{{ getPhrase('PRODUCTS PRICE :', 'upper') }} <span>{{ currency( Cart::instance('default')->subtotal() ) }}</span></p>
					<p class="fee">{{ getPhrase('Tax :', 'upper') }} <span>{{ currency( Cart::instance('default')->tax() ) }}</span></p>
					<?php
					$licence_price = 0;
					?>
					@if(session()->has('licence_price'))
					<p class="fee">{{ getPhrase('SUPPORT FEE :', 'upper') }} <span>{{ currency( session()->get('licence_price') ) }}</span></p>
					<?php $licence_price = session()->get('licence_price');?>
					@endif
					
					<?php
					$discount_amount = 0;
					?>
					@if(session()->has('discount_amount'))
					<p class="fee">{{ getPhrase('COUPON CODE OFF :', 'upper') }} <span>{{ currency( session()->get('discount_amount') ) }}</span></p>
					<?php $discount_amount = session()->get('discount_amount');?>
					@endif
					<p class="fees">{{ getPhrase('TOTAL PRICE :', 'upper') }} <span>{{ currency( Cart::total() + $licence_price - $discount_amount ) }}</span></p>

				</div>
				<div class="cart-button">
						<button type="submit" class="btn btn-primary">{{ getPhrase('CHECK OUT') }}</button>
				</div>
			</div>
			@else
				<div class="alert alert-danger">
				{{ getPhrase('Your cart is empty') }}
				</div>
			@endif
			<a href="{{ URL_DISPLAY_PRODUCTS }}" class="btn btn-primary btn-lg">{{ getPhrase('Continue Shopping') }}</a>
			@if (sizeof(Cart::content()) > 0)
			&nbsp;
            <a href="{{ URL_CHECKOUT }}" class="btn btn-success btn-lg">{{ getPhrase('Proceed to Checkout') }}</a>
			@endif
		</div>
	</div>
</section>
<!--/SECTION cart-->
@endsection