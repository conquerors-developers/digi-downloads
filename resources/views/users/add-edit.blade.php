@extends($layout)

@section('content')
 <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{getPhrase('users_management')}}
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{PREFIX}}"><i class="fa fa-dashboard"></i>{{getPhrase('home')}}</a></li>
        <li><a href="{{URL_USERS}}"> {{getPhrase('users')}}</a></li>
        <li class="active">{{$title}}</li>
      </ol>

    </section>

 <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-10 col-md-offset-1">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">{{$title}}</h3>
            </div>

            @include('errors.errors')
            <!-- /.box-header -->

            <?php $button_name = 'Create'; ?>
@if ($record)
<?php $button_name = 'Update'; ?>
{{ Form::model($record, 
array('url' => URL_USERS_EDIT.$record->slug, 
'method'=>'patch','novalidate'=>'','name'=>'formUsers ', 'files'=>'true' )) }}
@else
{!! Form::open(array('url' => URL_USERS_ADD, 'method' => 'POST', 'novalidate'=>'','name'=>'formUsers ', 'files'=>'true')) !!}
@endif

@include('users.form_elements', array('button_name'=> $button_name, 'record' => $record, 'roles'=>$roles))

{!! Form::close() !!}
          
           
          </div>
          <!-- /.box -->
 

        </div>
        <!--/.col (left) -->
      
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@stop

@section('footer_scripts')
@include('common.validations')
@stop