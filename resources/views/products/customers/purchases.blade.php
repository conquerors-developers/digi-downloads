@extends($layout)
@section('header_scripts')
<link href="{{CSS}}ajax-datatables.css" rel="stylesheet">
@stop
@section('content')

 <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="row">
  <div class="col-lg-12">
    <ol class="breadcrumb">
      <li><a href="{{URL_DASHBOARD}}"><i class="fa fa-home"></i> {{ getPhrase('Home') }}</a> </li>
      <li><a  href="{{URL_ADMIN_USERS_DASHBOARD}}">{{ getPhrase('users_dashboard')}}</a></li>         
      <li><a  href="{{URL_USERS_CUSTOMER_DETAILS.$record->slug}}">{{$record->name}} {{ getPhrase('details')}}</a></li>         
      <li class="active">{{isset($title) ? $title : ''}}</li>
    </ol>
  </div>
</div>
    </section>

  <div class="col-lg-12">
  <h3>{{$record->name}} {{getPhrase('purchases')}}</h3>
</div>
      <!-- Main content -->
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
      
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover datatable">
                <thead>
                <tr>
					<th>{{getPhrase('product_name')}}</th>
					{{-- <th>{{getPhrase('image')}}</th> --}}
					<th>{{getPhrase('cost')}}</th>
					<th>{{getPhrase('coupon_applied')}}</th>
					<th>{{getPhrase('paid_amount')}}</th>
					<th>{{getPhrase('payment_gateway')}}</th>
					<th>{{getPhrase('payment_status')}}</th>
					<th>{{getPhrase('date')}}</th>
					<th>{{getPhrase('customer_email')}}</th>
					
                </tr>
                </thead>
                <tbody>
                
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
 
           
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

    <!-- /.content -->

 @endsection
 
@section('footer_scripts')
@include('common.datatables',array('route'=>URL_CUSTOMERS_PURCHASES_PRODUCTS_LIST.$user_slug,'route_as_url'=>TRUE)) 
{{-- @include('common.deletescript', array('route' => URL_PRODUCTS_DELETE)) --}}
@stop
