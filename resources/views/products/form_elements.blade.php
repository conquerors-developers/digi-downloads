     <div class="box-body">
		<div class="col-md-6">                
			<div class="form-group">			
			 {{ Form::label('name', getPhrase( 'name' ) ) }} {!! required_field(); !!}
			 
			 {{ Form::text('name', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 'Product Name',

							'ng-model'=>'name',
							
							'ng-pattern' => getRegexPattern('name'),

							'required'=> 'true', 

							'ng-class'=>'{"has-error": formName.name.$touched && formName.name.$invalid}',

							'ng-minlength' => '2',

							'ng-maxlength' => '120',

							)) }}
			<div class="validation-error" ng-messages="formName.name.$error" >
				{!! getValidationMessage()!!}
				{!! getValidationMessage('pattern')!!}
				{!! getValidationMessage('minlength')!!}
				{!! getValidationMessage('maxlength')!!}
			</div>
			</div>
		</div>
		
		<div class="col-md-6">                
			<div class="form-group">			
			 {{ Form::label('product_format', getPhrase( 'format' ) ) }} {!! required_field(); !!}
			 
			 {{ Form::text('product_format', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 'Product format',

							'ng-model'=>'product_format',
							
							'ng-pattern' => getRegexPattern('name'),

							'required'=> 'true', 

							'ng-class'=>'{"has-error": formName.product_format.$touched && formName.product_format.$invalid}',

							'ng-minlength' => '2',

							'ng-maxlength' => '120',

							)) }}
			<div class="validation-error" ng-messages="formName.product_format.$error" >
				{!! getValidationMessage()!!}
				{!! getValidationMessage('pattern')!!}
				{!! getValidationMessage('minlength')!!}
				{!! getValidationMessage('maxlength')!!}
			</div>
			</div>
		</div>

		<div class="col-md-6">							
			<?php 
			$status = array();
			$status['Active'] = getPhrase( 'Active' );
			$status['Inactive'] = getPhrase('Inactive');
			?>
			<div class="form-group">
			{{ Form::label('status', getPhrase( 'Status' ) ) }}
			{{Form::select('status', $status, null, ['class'=>'form-control', "id"=>"status", 'data-toggle' => 'tooltip'])}}
			</div>	   
		</div>
		
		<div class="col-md-6">							
			<?php 
			$status = array();
			$status['no'] = getPhrase('No');
			$status['yes'] = getPhrase( 'Yes' );			
			?>
			<div class="form-group">
			{{ Form::label('is_featured', getPhrase( 'is_featured' ) ) }}
			{{Form::select('is_featured', $status, null, ['class'=>'form-control', "id"=>"is_featured", 'data-toggle' => 'tooltip'])}}
			</div>	   
		</div>
		
		<div class="col-md-12">
			<h2>{{ getPhrase('Categories') }}</h2>
			<div class="form-group">			
			<?php $count = 0;
			$selected_cats = array();
			if( $record ) {
				$selected_cats = (array) json_decode($record->categories);
			}
			?>			
			{{Form::select('categories[]', $categories, $selected_cats, ['data-toggle' => 'tooltip', 'title' => getPhrase('Please select categorie(s)'), 'class' => 'form-control select2', 'multiple' => 'multiple',])}}
			</div>
		</div>
		
		<div class="col-md-12">
			<h2>{{ getPhrase('Licences') }}</h2>
			<div class="form-group">			
			<?php $count = 0;
			$selected_cats = array();
			if( $record ) {
				$selected_cats = (array) json_decode($record->licences);
			}
			$licences = array_pluck(App\Licence::where('status', '=', 'Active')->get(), 'title', 'id');
			?>			
			{{Form::select('licences[]', $licences, $selected_cats, ['data-toggle' => 'tooltip', 'title' => getPhrase('Please select categorie(s)'), 'class' => 'form-control select2', 'multiple' => 'multiple',])}}
			</div>
		</div>
		
		<div class="col-md-6">							
			<h2>{{ getPhrase('Price Settings') }}</h2>
			<?php 
			$price_types = array(
				'default' => getPhrase( 'Default' ),
				'variable' => getPhrase('Variable'),
			);
			?>
			<div class="form-group">
			{{ Form::label('price_type', getPhrase( 'price_type' ) ) }}
			{{Form::select('price_type',$price_types, null, ['class'=>'form-control', 'id' => 'price_type', 'data-toggle' => 'tooltip', 'required' => 'true'])}}
			</div>
			<div class="validation-error" ng-messages="formName.price_type.$error" >
				{!! getValidationMessage()!!}
			</div>
		</div>
		<div class="col-md-6" id="fixedprice_options_div">
			<h2>&nbsp;</h2>
			<div class="form-group">
			 {{ Form::label('price', getPhrase( 'price' ) ) }} {!! required_field(); !!}
			 {{ Form::number('price', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase( 'Eg: 2' ), 'required' => true, 'title' => getPhrase('Price'), 'data-toggle' => 'tooltip')) }}
			</div>
		</div>
		<div class="col-md-6" id="price_display_type_div">
			<h2>&nbsp;</h2>
			<div class="form-group">
			 {{ Form::label('price_display_type', getPhrase( 'price_display_type' ) ) }} {!! required_field(); !!}
			 {{ Form::select('price_display_type', array('checkbox' => getPhrase('Checkbox'), 'radio' => getPhrase('Radio') ), null, ['class'=>'form-control', 'id' => 'price_type', 'data-toggle' => 'tooltip', 'required' => 'true']) }}
			</div>
		</div>
		<div class="col-mod-12" id="variableprice_options_div" style="display:none;">
			<div id="digi_price_fields" class="edd_meta_table_wrap">
				<table class="widefat digi_repeatable_table" width="100%" cellpadding="0" cellspacing="0">
					<thead>
						<tr>
							<th style="width: 20px"></th>
							<th>{{ getPhrase('Option Name') }}</th>
							<th>{{ getPhrase('Price') }}</th>
							<th>{{ getPhrase('Default') }}</th>
							<th>{{ getPhrase('ID') }}</th>
							<th></th>
						</tr>
					</thead>
					<tbody>					
						<?php
						$prices_options = array('All');
						if( $record ) {
							$old_prices = json_decode( $record->price_variations );
							if( ! empty( $old_prices ) ) {
								foreach( $old_prices as $key => $value ) {
									$value = ( array ) $value;
									$name   = isset( $value['name'] )   ? $value['name']   : '';
									$amount = isset( $value['amount'] ) ? $value['amount'] : '';
									$index  = isset( $value['index'] )  ? $value['index']  : $key;
									$isdefault = isset( $value['isdefault'] ) ? $value['isdefault'] : false;
									?>
									<tr class="digi_variable_prices_wrapper digi_repeatable_row" data-key="1">
										@include('products.price_row', array('index' => $index, 'key' => $key, 'name' => $name, 'amount' => $amount, 'isdefault' => $isdefault, 'record' => $record))
									</tr>
									<?php
									$prices_options[] = $name;
								}
							}
						} else {
						?>
						<tr class="digi_variable_prices_wrapper digi_repeatable_row" data-key="1">
							@include('products.price_row', array('index' => 1, 'key' => 1, 'name' => '', 'amount' => '', 'isdefault' => false, 'record' => $record))
						</tr>
						<?php } ?>
						<tr>
							<td class="submit" colspan="4" style="float: none; clear:both; background:#fff;">
								<button class="button-secondary digi_add_repeatable" style="margin: 6px 0;">{{ getPhrase('Add New Price') }}</button>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		
		<div class="col-md-12">
			<h2>{{ getPhrase('Download Files') }}</h2>
		</div>
		
		<div class="col-md-12" id="downloadfiles_options_div">
			<div id="digi_file_fields" class="digi_meta_table_wrap">
				<table class="widefat digi_repeatable_table" width="100%" cellpadding="0" cellspacing="0">
					<thead>
						<tr>
							<th style="width: 20px"></th>
							<th style="width: 20%">{{ getPhrase('File Name') }}</th>
							<th>{{ getPhrase('Type') }}</th>
							<th>{{ getPhrase('File URL') }}</th>
							<th class="pricing" style="width: 20%;">{{ getPhrase('Price Assignment') }}
							</th>
							<th style="width: 15px">{{ getPhrase('ID') }}</th>
							<th style="width: 2%"></th>
						</tr>
					</thead>
					<tbody>						
						<?php						
						if( $record ) {
							$old_files = json_decode( $record->download_files );
							if( ! empty( $old_files ) ) {
								foreach( $old_files as $key => $value ) {
									$value = ( array ) $value;
									$name   = isset( $value['name'] )   ? $value['name']   : '';
									$type   = isset( $value['type'] )   ? $value['type']   : 'file';
									$file_name = isset( $value['file_name'] ) ? $value['file_name'] : '';
									$index  = isset( $value['index'] )  ? $value['index']  : $key;
									$option = isset( $value['option'] ) ? $value['option'] : 'All';
									?>
									<tr class="digi_repeatable_upload_wrapper digi_repeatable_row" data-key="1">
										@include('products.downloadfile', array('key' => $key, 'index' => $index, 'prices' => $prices_options, 'name' => $name, 'type' => $type, 'file_name' => $file_name, 'option' => $option, 'record' => $record))
									</tr>
									<?php
								}
							}
						} else {
						?>
						<tr class="digi_repeatable_upload_wrapper digi_repeatable_row" data-key="1">
							@include('products.downloadfile', array('key' => 1, 'index' => 0, 'prices' => $prices_options, 'name' => '', 'type' => 'file','file_name' => '', 'option' => 'All', 'record' => $record))
						</tr>
						<?php } ?>
						<tr>
							<td class="submit" colspan="4" style="float: none; clear:both; background: #fff;">
								<button class="button-secondary digi_add_repeatable" style="margin: 6px 0 10px;">{{ getPhrase('Add New File') }}</button>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		
		<div class="col-md-12">                
			<div class="form-group">
			 {{ Form::label('download_limits', getPhrase( 'download_limits' ) ) }}
			 {{ Form::number('download_limits', $value = 0 , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase( 'Leave blank for global setting or 0 for unlimited' ), 'title' => getPhrase('Limit the number of times a customer who purchased this product can access their download links.'), 'data-toggle' => 'tooltip')) }}
			</div>
		</div>
		
		<div class="col-md-6">                
			<div class="form-group">
			 {{ Form::label('demo_link', getPhrase( 'Demo Link' ) ) }}
			 {{ Form::text('demo_link', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('Demo Link Eg: http://site.com'), 'data-toggle' => 'tooltip')) }}
			</div>
		</div>
		<div class="col-md-6">                
			<div class="form-group">
			 {{ Form::label('image', getPhrase( 'Preview image' ) ) }}
			 {{ Form::file('image', $value = null , $attributes = array('class'=>'form-control', 'data-toggle' => 'tooltip')) }}
			 <?php
			 if( $record ) {
				 echo '<img src="'.UPLOAD_URL_PRODUCTS_THUMBNAIL.$record->image.'" alt="'.$record->name.'" title="'.$record->name.'">';
			 }
			 ?>
			</div>
		</div>
			
		<div class="col-md-12">
			<div class="form-group">
				{{ Form::label('licence_of_use', getPhrase( 'licence_of_use' ) ) }}               
				{{ Form::textarea('licence_of_use', $value = null, $attributes = array('class'=>'form-control ckeditor', 'placeholder' => getPhrase( 'licence_of_use for the product' ), 'rows'=>'4', 'data-toggle' => 'tooltip', 'title' => getPhrase('licence_of_use for the product'))) }}
			</div>
			<div class="form-group">
				{{ Form::label('technical_info', getPhrase( 'technical_info' ) ) }}               
				{{ Form::textarea('technical_info', $value = null, $attributes = array('class'=>'form-control ckeditor', 'placeholder' => getPhrase( 'technical_info for the product' ), 'rows'=>'4', 'data-toggle' => 'tooltip', 'title' => getPhrase('technical_info for the product'))) }}
			</div>
			<div class="form-group">
				{{ Form::label('description', getPhrase( 'description' ) ) }}               
				{{ Form::textarea('description', $value = null, $attributes = array('class'=>'form-control ckeditor', 'placeholder' => getPhrase( 'Description for the product' ), 'rows'=>'4', 'data-toggle' => 'tooltip', 'title' => getPhrase('Description for the product'))) }}
			</div>
			<div class="form-group">
				{{ Form::label('download_notes', getPhrase( 'download_notes' ) ) }}               
				{{ Form::textarea('download_notes', $value = null, $attributes = array('class'=>'form-control ckeditor', 'placeholder' => getPhrase( 'Download notes for the product' ), 'rows'=>'4', 'data-toggle' => 'tooltip', 'title' => getPhrase('Download notes for the product'))) }}
			</div>
			<h2>SEO Settings</h2>
			<div class="form-group">
			 {{ Form::label('meta_tag_title', getPhrase( 'Title Meta Tag' ) ) }}
			 {{ Form::text('meta_tag_title', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase( 'Title Meta Tag' ), 'data-toggle' => 'tooltip', 'title' => getPhrase('Product SEO Title'))) }}
			</div>
		</div>
			
			<div class="col-md-6">
				<div class="form-group">
				{{ Form::label('meta_tag_description', getPhrase( 'Description Meta Tag' ) ) }}               
				{{ Form::textarea('meta_tag_description', $value = null, $attributes = array('class'=>'form-control ckeditor', 'placeholder' => getPhrase( 'Description Meta Tag' ), 'rows'=>'4', 'data-toggle' => 'tooltip')) }}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
				{{ Form::label('meta_tag_keywords', getPhrase( 'Kewords Meta Tag' ) ) }}               
				{{ Form::textarea('meta_tag_keywords', $value = null, $attributes = array('class'=>'form-control ckeditor', 'placeholder' => getPhrase( 'Kewords Meta Tags separated with comma(,)' ), 'rows'=>'4', 'data-toggle' => 'tooltip')) }}
				</div>
			</div>
              </div>   
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right" >{{$button_name}}</button>
              </div>