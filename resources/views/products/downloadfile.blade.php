<td>
	<span class="digi_draghandle"></span>
	<input type="hidden" name="digi_download_files[<?php echo $key; ?>][index]" class="digi_repeatable_index" value="<?php echo $index; ?>"/>
</td>
<td>
	<input type="text" name="digi_download_files[<?php echo $key; ?>][name]" class="digi_repeatable_name_field large-text" value="{{ $name }}" placeholder="File Name"/>
</td>
<td>
	<select name="digi_download_files[<?php echo $key; ?>][type]" title="{{ getPhrase('Please select type') }}" class="upload_type" data-index="{{ $index }}">
		<option value="file" <?php if( $type == 'file') echo 'selected';?>>{{ getPhrase('File') }}</option>
		<option value="url" <?php if( $type == 'url') echo 'selected';?>>{{ getPhrase('URL', 'upper') }}</option>
	</select>
</td>

<td>
	<div class="digi_repeatable_upload_field_container">
		<?php
		$field_type = $type;
		if( $field_type == 'url' )
			$field_type = 'text';
		?>
		<input type="{{ $field_type }}" name="digi_download_files[<?php echo $key; ?>][file]" class="digi_repeatable_upload_field digi_upload_field large-text digi_upload_file_button digi_upload_file_button_index_{{ $index }}" value="{{ $file_name }}" placeholder="URL Eg: http://site.com"/>
		<?php
		if( $type == 'file' && $file_name != '' ) {
			echo '<a href="'.UPLOAD_URL_PRODUCTS_DOWNLOADS.$file_name.'" target="_blank">View</a>';
		}
		?>
	</div>
</td>

<td class="pricing">
	<select name="digi_download_files[<?php echo $key;?>][condition]" class="digi_repeatable_condition_field">
	<option value="">Please select</option>
	<?php
		$options = array();
		if ( $prices ) {
			foreach ( $prices as $price_key  ) {
				if( $price_key == $option ) {
					echo '<option value="'.$price_key.'" selected>' . $price_key . '</option>';
				} else {
					echo '<option value="'.$price_key.'">' . $price_key . '</option>';
				}
			}
		}
	?>
	</select>
</td>

<td>
	<span class="digi_file_id"><?php echo $key; ?></span>
</td>

<td>
	<button class="digi_remove_repeatable" data-type="file" style="background: url(<?php echo IMAGES . '/xit.gif'; ?>) no-repeat;"><span class="screen-reader-text"><?php printf( 'Remove file option %s', $key ); ?></span><span aria-hidden="true">&times;</span></button>
</td>