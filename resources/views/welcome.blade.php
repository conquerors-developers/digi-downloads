@extends('layouts.layout-site')

@section('header_scripts')
<link rel="stylesheet" href="{{CSS}}select2.css">
@stop

@section('content')
<!--SECTION-1 Search goods-->
<section class="search-bg">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1 class="hero-title"> {{ getPhrase('home_page_banner_heading') }}</h1>
				<p class="search-para">{{ getPhrase('home_page_banner_sub_heading') }}</p>
				<div class="sign">					
						{!! Form::open(array('url' => URL_INDEX_SEARCHPRODUCT, 'method' => 'POST', 'name'=>'fromSearch ',  )) !!}
						<div class="input-group">
							<div class="input-group-btn ">
								<span class=" fa-btn "><i class="fa fa-search hidden-xs"></i></span>
							</div>
							<?php
							$searchproducts = App\Product::where('status', '=', 'Active')->orderBy('created_at', 'desc');
							?>
							<div class="dropdown">
							<input type="text" name="title" id="title" class="form-control" placeholder="{{ getPhrase('Search') }}" data-toggle="dropdown">								
								<ul class="dropdown-menu search-dropmenu" id="products_dropdown">
									@if( $searchproducts->count() > 0 )
										@foreach( $searchproducts->paginate(10) as $product )
										<?php
										$price = $product->price;
										if($product->price_type == 'variable') {
											$price_variations = json_decode( $product->price_variations );
											$min_price = $max_price = $index = 0;
											if( ! empty( $price_variations ) ) {
												foreach( $price_variations as $key => $item ) {
													if( $index == 0)
														$min_price = $item->amount;
													if( $item->amount < $min_price )
														$min_price = $item->amount;
													if( $item->amount > $max_price )
														$max_price = $item->amount;
													$index++;
												}
											}
										}
										?>
										<li>
											   <a href="{{ URL_DISPLAY_PRODUCTS_DETAILS . $product->slug }}">
											   <div class="media-left">
											   <?php
												$product_image = DEFAULT_PRODUCT_IMAGE_THUMBNAIL;
												if( $product->image != '' && File::exists(UPLOAD_PATH_PRODUCTS_THUMBNAIL . $product->image ) ) {
													$product_image = UPLOAD_URL_PRODUCTS_THUMBNAIL . $product->image;
												}
												?>
											   <img src="{{ $product_image }}"></div>
												<div class="media-body">
													<h4>{{ $product->name }}</h4>
													<p>
													@if( $product->price_type == 'default' )
														{{ currency( $price) }}
													@else
														{{ currency( $min_price ) }} - {{ currency( $max_price ) }}
													@endif
													</p>
												</div>
												</a>
										</li>
										@endforeach
									@endif		
									
								</ul>
							
							</div>

							<?php
							$categories = App\Category::where('status', '=', 'Active');
							?>
							@if( $categories->count() > 0 )
							<div class="input-group-btn">
								<span class="select">
									<select class="btn btn-select select2" name="category">
										<option value="0">{{ getPhrase('search_in_all_categories') }}</option>
										@foreach( $categories->get() as $category)
											@if( $category->parent_id == 0 )
												<?php
												$subcats = App\Category::where('status', '=', 'Active')->where('parent_id', '=', $category->id);
												?>
												<option value="{{ $category->slug}}">{{ $category->title}}</option>
												@if( $subcats->count() > 0 )
													<optgroup label="{{ $category->title}}">
													@foreach( $subcats->get() as $subcat)
														<option value="{{ $subcat->slug}}">{{ $subcat->title}}</option>
													@endforeach
													</optgroup>
												@else												
												<option value="{{ $category->slug}}">{{ $category->title}}</option>
												@endif
											@endif
										@endforeach
									</select>
								</span>
							</div>
							@endif
							<!-- /btn-group -->

							<div class="input-group-btn">
								<button class="btn btn-primary btn-search" type="submit"><span class="hidden-xs">{{ getPhrase('SEARCH') }}</span><span class="visible-xs fa fa-search"></span></button>
							</div>
						</div>
					</form>
				</div>
				<h3>UI Kits, Themes, HTML Templates Strarting From $5</h3>
			</div>
		</div>
	</div>
</section>
<!--/SECTION-1 Search goods-->

<!--section 2-OFFER-->
<?php $offer = App\Offers::select('offers.*', 'products.name', 'products.image as product_image', 'products.price', 'products.price_type', 'products.price_variations', 'products.description as product_description', 'products.id as product_id', 'products.slug as product_slug')->join('products', 'products.id', '=', 'offers.product_id')->where('offers.status', '=', 'active')->whereRaw('"'.date('Y-m-d H:i:s') . '" BETWEEN start_date_time and end_date_time')->orderBy('start_date_time', 'desc')->first();
?>
@if( $offer )
<section class="offer">
	<div class="container">
		<div class="row">
			<div class="div col-md-12">
				<h3 class="heading">{{ getPhrase('today_offer', 'upper') }}</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-5  col-sm-6 ">
				<?php
				$bg = PREFIX . 'images/offerbg.png';
				if( $offer->product_image == 'yes' ) {
					if( file_exists(UPLOAD_PATH_PRODUCTS_THUMBNAIL . $offer->product_image) ) {
						$bg = UPLOAD_URL_PRODUCTS_THUMBNAIL . $offer->product_image;
					}
				}
				?>
				<img src="{{ $bg }}" alt="" class="img-responsive">
			</div>
			<div class="col-md-7 col-sm-6">
				<?php
				$title = $offer->title;
				$description = $offer->description;
				if( $offer->use_product_title == 'yes' ) {
					$title = getPhrase('Get ') . '<span>'.$offer->name . '</span>' . getPhrase(' for just ') .  '<span>'.currency( $offer->price ) . '</span>';
				}
				if( $offer->use_product_description == 'yes' ) {
					$description = $offer->product_description;
				}
				?>
				<h2>{!! $title !!}</h2>
					{!! $description !!}

				<!--for countdown timer-->
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div id="main">
							<div class="counter">
								<h6>{{ getPhrase('This offer expires on') }}</h6>
								<div id="countdowner">

								</div>
								<!-- /#Countdown Div -->
								<ul>
									<li>{{ getPhrase('Days') }}</li>
									<li>{{ getPhrase('Hours') }}</li>
									<li>{{ getPhrase('Mins') }}</li>
									<li>{{ getPhrase('Sec') }}</li>
								</ul>
							</div>
							<!-- /.Counter Div -->

						</div>
						<!-- /#Main Div -->
					</div>
					<!-- /.Columns Div -->
				</div>
				<!-- /.Row Div -->



				<!--for buttons-->
				<form action="{{ URL_DISPLAY_PRODUCTS_CART }}" method="POST" class="side-by-side">
				{!! csrf_field() !!}
					<div class="buttons">
						<a href="{{ URL_DISPLAY_PRODUCTS_DETAILS . $offer->product_slug }}" class="btn btn-primary">{{ getPhrase('VIEW DETAILS', 'upper') }}</a> &nbsp;
						@if( $offer->price_type == 'default')
							<input type="hidden" name="id" value="{{ $offer->product_id }}">
							<input type="hidden" name="name" value="{{ $offer->name }}">
							<input type="hidden" name="price" value="{{ $offer->price }}">
							<button type="submit" class="btn btn-default">{{ getPhrase('BUY ') . currency( $offer->price )							}}</button>
						@endif
					</div>
				</form>
			</div>
		</div>
	</div>

</section>
@endif
<!--/section 2-OFFER-->

<!--section 3-PRODUCTS-->
<div class="grey-bg">
	<div class="container">
		<div class="">
			<div class="row cs-row">
				<div class="col-md-12">
					<h2 class="heading heading-center">{{ getPhrase('PRODUCTS', 'upper') }}</h2>
					<div class="products">
						<div class="tabs">
							<ul class="list-inline">
								<li><a href="javascript:void(0);" onclick="javascript:getProducts( 'popular')">{{ getPhrase('Popular') }}</a></li>
								<li><a href="javascript:void(0);" onclick="javascript:getProducts( 'featured')">{{ getPhrase('Featured') }}</a></li>
								<li><a href="javascript:void(0);" onclick="javascript:getProducts( 'latest')">{{ getPhrase('Latest') }}</a></li>
								<li><a href="javascript:void(0);" onclick="javascript:getProducts( 'free')">{{ getPhrase('Freebies') }}</a></li>
							</ul>
						</div>
					</div>
				</div>				
				<div class="row" id="products">
				@include('displayproducts.products', array('products' => $products, 'nopagelinks' => 'nopagelinks', 'type' => 'related'))
				</div>
				<div class="col-lg-12">
					<div class="products-button">						
						<a class="btn btn-primary" href="{{ URL_DISPLAY_PRODUCTS }}">{{ getPhrase('SEE ALL PRODUCTS', 'upper') }}</a>
					</div>
				</div>

			</div>

		</div>
	</div>
</div>
<!--/section 3-PRODUCTS-->

<!--section-4 CATEGORIES-->
@include('displayproducts.products-view', array('products' => $category_products))
<!--/section-4 CATEGORIES-->

<!--SECTION-5 SIGN UP-->
@include('common.subscrption-form')
<!--/SECTION-5 SIGN UP-->
@endsection

@section('footer_scripts')
	@include('common.select2')
	<script type="text/javascript">
	$('#title').on( 'keydown', function(){
			$('#products_dropdown').hide();
	});
	
	function getProducts(param) {
        $.ajax({
            url : '?param=' + param,
            dataType: 'html',
        }).done(function (data) {
            $('#products').html(data);			
			//window.location.hash = '?page=' + page + '&param=' + param;
			//location.hash = page;
        }).fail(function () {
            alert('Posts could not be loaded.');
        });
    }
	</script>
	@include('common.subscribe')
@endsection