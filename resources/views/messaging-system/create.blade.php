@extends($layout)

@section('content')
{{-- <link rel="stylesheet" type="text/css" href="{{CSS}}select2.css"> --}}
@if(checkRole(getUserGrade(7)))
   
    <section class="login-banner">
        <div class="container">
            <div class="row">
                <div class="div col-sm-12">
                    <h2>{{ Auth::user()->name }}</h2>
                </div>
            </div>
        </div>
    </section>

 <section class="dashboard2">
<div class="container">  
                <!-- Page Heading -->
               
<!-- <h1>Create a new message</h1> -->
<div class="panel panel-custom">
                    <div class="panel-heading">
                    <div class="pull-right messages-buttons">
                            <a class="btn btn-lg btn-info button" href="{{URL_MESSAGES}}"> {{getPhrase('inbox').'('.$count = Auth::user()->newThreadsCount().')'}} </a>
                            <a class="btn btn-lg btn-info button" href="{{URL_MESSAGES_CREATE}}"> 
                            {{getPhrase('compose')}}</a>

                 
                        </div>
                        <h1>{{$title}}</h1>
                    </div>

                    <div class="panel-body packages">
                         
                        <div class="row library-items">

{!! Form::open(['route' => 'messages.store']) !!}
<div class="col-md-6 col-md-offset-3">
<?php $tosentUsers = array(); ?>
 @if($users->count() > 0)
    
        <?php foreach($users as $user) {
                $tosentUsers[$user->id] = $user->name; 
            }
        ?>
     {!! Form::label('Select User', 'Select User', ['class' => 'control-label']) !!}
    {{Form::select('recipients[]', $tosentUsers, null, ['class'=>'form-control select2', 'name'=>'recipients[]', 'multiple'=>'true'])}}

 @endif
 
    
    <!-- Subject Form Input -->
    <div class="form-group">
        {!! Form::label('subject', 'Subject', ['class' => 'control-label']) !!}
        {!! Form::text('subject', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Message Form Input -->
    <div class="form-group">
        {!! Form::label('message', 'Message', ['class' => 'control-label']) !!}
        {!! Form::textarea('message', null, ['class' => 'form-control']) !!}
    </div>

   
    
    <!-- Submit Form Input -->
    <div class="text-right">
        {!! Form::submit('Submit', ['class' => 'btn btn-primary btn-lg']) !!}
    </div>
</div>
{!! Form::close() !!}
  </div>
                </div>
            
            
</div>
</div>
</section>

@else
<div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <section class="content-header">
                <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <li><a href="{{URL_DASHBOARD}}"><i class="fa fa-home"></i> {{getPhrase('home')}}</a> </li>
                            <li><a href="{{URL_MESSAGES}}">{{getPhrase('messages')}}</a> </li>

                            <li class="active"> {{ $title }} </li>
                        </ol>
                    </div>
                </div>
                </section>
<!-- <h1>Create a new message</h1> -->
<div class="panel panel-custom">
                    <div class="panel-heading">
                    <div class="pull-right messages-buttons">
                            <a class="btn btn-lg btn-info button" href="{{URL_MESSAGES}}"> {{getPhrase('inbox').'('.$count = Auth::user()->newThreadsCount().')'}} </a>
                            <a class="btn btn-lg btn-info button" href="{{URL_MESSAGES_CREATE}}"> 
                            {{getPhrase('compose')}}</a>

                 
                        </div>
                        <h1>{{$title}}</h1>
                    </div>

                    <div class="panel-body packages">
                         
                        <div class="row library-items">

{!! Form::open(['route' => 'messages.store']) !!}
<div class="col-md-6 col-md-offset-3">
<?php $tosentUsers = array(); ?>
 @if($users->count() > 0)
    
        <?php foreach($users as $user) {
                $tosentUsers[$user->id] = $user->name; 
            }
        ?>
     {!! Form::label('Select User', 'Select User', ['class' => 'control-label']) !!}
    {{Form::select('recipients[]', $tosentUsers, null, ['class'=>'form-control select2', 'name'=>'recipients[]', 'multiple'=>'true'])}}

 @endif
 
    
    <!-- Subject Form Input -->
    <div class="form-group">
        {!! Form::label('subject', 'Subject', ['class' => 'control-label']) !!}
        {!! Form::text('subject', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Message Form Input -->
    <div class="form-group">
        {!! Form::label('message', 'Message', ['class' => 'control-label']) !!}
        {!! Form::textarea('message', null, ['class' => 'form-control']) !!}
    </div>

   
    
    <!-- Submit Form Input -->
    <div class="text-right">
        {!! Form::submit('Submit', ['class' => 'btn btn-primary btn-lg']) !!}
    </div>
</div>
{!! Form::close() !!}
  </div>
                </div>
            </div>
            
</div>
</div>

@endif
@stop
 
@section('footer_scripts')
    
    <script src="{{JS}}select2.js"></script>
    
    <script>
      $('.select2').select2({
       placeholder: "Add User",
    });
    </script>
@stop