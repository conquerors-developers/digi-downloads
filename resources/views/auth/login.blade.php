@extends('layouts.layout-site')

@section('content')
<!--Inner Banner-->
<section class="login-banner">
	<div class="container">
		<div class="row">
			<div class="div col-sm-12">
				<h2>{{ getPhrase('LOGIN HERE') }}</h2>
			</div>
		</div>
	</div>
</section>
<!--/Inner Banner-->

<!--SECTION LOGIN-->
<section class="login">
	<div class="container">
		<div class="row">
		<div class="col-sm-3"></div>
			<div class="col-sm-6">
				<h2 class="heading heading-center">{{ getPhrase('GO TO MY ACCOUNT') }}</h2>
				@include('errors.errors')
				@if (session()->has('success_message'))
					<div class="alert alert-success">
						{{ session()->get('success_message') }}
					</div>
				@endif

				@if (session()->has('error_message'))
					<div class="alert alert-danger">
						{{ session()->get('error_message') }}
					</div>
				@endif
				<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}" id="myForm">
					{{ csrf_field() }}
					<div class="form-group">
						
						<input type="email"  name="email" id="email" class="form-control" placeholder="Email Address">						
						
					</div>
					<div class="form-group">
						<input type="password" name="password" id="password"  class="form-control" placeholder="Password">
					</div>
                    <div class="checkbox">
                        <label><input type="checkbox" value="">Remember Me?</label>
                    </div>
					<div class="logbtn animated fadeInDown">
						<button type="submit" class="btn btn-default">Log In</button>

						<p class="log"><a href="{{ url('/password/reset') }}">Forgot Password?</a><br></p>
					</div>
					<div class="regbtn animated fadeInDown">
						<p >Dont have account? Create Now</p>
						<a href="{{ url('/register') }}" class="btn btn-default">REGISTER</a>
					</div>
				</form>
			</div>
            <div class="col-sm-3"></div>
		</div>
	</div>
</section>

<!--/SECTION LOGIN-->
@endsection

@section('footer_scripts')
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$('#myForm').validate({
			rules:{
				email:{
					required: true,
					email: true
				},
				password: {
					required: true
				}
			},
			messages: {
				email:{
					'required': "{{getPhrase('Please enter email address')}}",
					'email': "{{ getPhrase('Please enter valid email address')}}"
				},
				password:{
					'required': "{{ getPhrase('Please enter password') }}"
				}
			}
		});
	});
</script>
@endsection