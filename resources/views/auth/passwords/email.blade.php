@extends('layouts.layout-site')

<!-- Main Content -->
@section('content')
<!--SECTION-1 BANNER-->
<section class="login-banner">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h2>{{ getPhrase('Reset Password') }}</h2>
			</div>
		</div>
	</div>
</section>
<!--/SECTION BANNER-->
<!--SECTION LOGIN-->
<section class="login">
	<div class="container">
		<h2>{{ getPhrase('GO TO MY ACCOUNT') }}</h2>
		<h6>{{ getPhrase('Please fill details to get password') }}</h6>
		<div class="form-group">
			@include('errors.errors')
		</div>
		<form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
		{{ csrf_field() }}
			<div class="form-group">
				<input id="email" type="email" class="form-control" name="email" placeholder="{{ getPhrase('Email address') }}" value="{{ old('email') }}" required>				
			</div>


			<div class="logbtn animated fadeInDown">
				<button type="submit" class="btn btn-default">{{ getPhrase('Send Password Reset Link') }}</button>
			</div>
		</form>

	</div>
</section>
<!--/SECTION LOGIN-->
@endsection
