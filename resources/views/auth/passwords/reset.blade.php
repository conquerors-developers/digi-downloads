@extends('layouts.layout-site')

@section('content')
<!--SECTION-1 BANNER-->
<section class="login-banner">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h2>{{ getPhrase('Reset Password') }}</h2>
			</div>
		</div>
	</div>
</section>
<!--/SECTION BANNER-->
<!--SECTION LOGIN-->
<section class="login">
	<div class="container">
		<h2>{{ getPhrase('GO TO MY ACCOUNT') }}</h2>
		<h6>{{ getPhrase('Please fill details to get password') }}</h6>
		
		@include('errors.errors')
		
		<form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
		{{ csrf_field() }}
		<input type="hidden" name="token" value="{{ $token }}">
			<div class="form-group">
				<input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" placeholder="{{ getPhrase('Email address') }}" required autofocus>
			</div>
			
			<div class="form-group">
				<input id="password" type="password" class="form-control" name="password" placeholder="{{ getPhrase('Password') }}" required>
			</div>
			
			<div class="form-group">
				<input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="{{ getPhrase('Confirm Password') }}" required>
			</div>


			<div class="logbtn animated fadeInDown">
				<button type="submit" class="btn btn-primary">{{ getPhrase('Reset Password') }}</button>
			</div>
		</form>

	</div>
</section>
<!--/SECTION LOGIN-->
@endsection
