@extends('layouts.layout-site')

@section('content')
<!--Inner Banner-->
<section class="login-banner">
	<h2>{{ getPhrase('REGISTER') }}</h2>
</section>
<!--/Inner Banner-->

<!--SECTION LOGIN-->
<section class="login">
	<div class="container">
		<div class="row">
		<div class="col-sm-3"></div>
			<div class="col-sm-6">
				<h2 class="heading heading-center">{{ getPhrase('GREAT, JOIN WITH US') }}</h2>
				<h6>{{ getPhrase('register_sub_heading') }}</h6>
				@include('errors.errors')
				@if( isset($role) && $role == 'vendor' )
					Click <a href="{{ URL_USERS_REGISTER }}">here</a> to register as customer
				@else
					Click <a href="{{ URL_USERS_REGISTER }}/vendor">here</a> to register as vendor
				@endif
				<form class="form-horizontal" role="form" method="POST" action="{{ URL_USERS_REGISTER }}" id="formName">
                        {{ csrf_field() }}
				
					<?php
					$role = isset($role) ? 'vendor' : 'customer';					
					?>
					<input name="role" type="hidden" value="{{ $role }}">
					<div class="form-group">
                        <input type="email" name="email"  id="email" class="form-control" placeholder="Email Address">
					</div>
					<div class="form-group">
                        <input type="text" name="name" id="name" class="form-control" placeholder="First Name">
					</div>
					<div class="form-group">
                        <input type="text"  name="name" id="name1" class="form-control" placeholder="Last Name">
					</div>				
					
					<div class="form-group">
                        <input type="password"  name="password" id="password" class="form-control" placeholder="Password">
					</div>
					<div class="form-group">
                        <input type="password"  name="password" id="password" class="form-control" placeholder="Re-enter Password">
					</div>
					<p class="terms">{{ getPhrase('By creating an account you agree to our') }}
						<?php
						$terms_and_conditions = getSetting('terms_and_conditions', 'site_settings');
						$privacy_policy = getSetting('privacy_policy', 'site_settings');
						if( $terms_and_conditions != '' || $privacy_policy != '' )
						{
						?>
						<br> 
						@if($terms_and_conditions != '')
							<a href="{{ $terms_and_conditions }}">{{ getPhrase('Terms and Conditions') }} </a>
						@endif
						@if($privacy_policy != '')
							{{ getPhrase('and our') }} <a href="{{ $privacy_policy }}">{{ getPhrase('Privacy Policy') }}</a>
						@endif
						<?php } ?>
						</p>
					<div class="reg-btn">
						<button type="submit" class="btn btn-default">{{ getPhrase('REGISTER') }} </button>
						<p class="reg-data">{{ getPhrase('Already having account?') }}
							<a href="{{ URL_USERS_LOGIN }}">{{ getPhrase('Login here') }}</a>
						</p>
					</div>
				</form>
			</div>
            <div class="col-sm-3"></div>
		</div>
	</div>
</section>
<!--/SECTION LOGIN-->
@endsection

@section('footer_scripts')
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	$('#formName').validate({
		rules:{
			first_name:{
				required: true
			},
			email:{
				required: true,
				email: true
			},
			password: {
				required: true,
				minlength:6
			},
			password_confirmation: {
				required: true,
				minlength:6,
				equalTo: '#password'
			}
		},
		messages: {
			first_name:{
				required: "{{getPhrase('Please enter first name')}}"
			},
			email:{
				required: "{{getPhrase('Please enter email address')}}",
				email: "{{getPhrase('Please enter valid email address')}}"
			},
			password:{
				required: "{{getPhrase('Please enter password')}}",
				minlength: "{{ getPhrase('Password should be at least 6 characters') }}"
			},
			password_confirmation:{
				required: "{{getPhrase('Please enter password again to confirm')}}",
				minlength: "{{ getPhrase('Password should be at least 6 characters') }}",
				equalTo: "{{ getPhrase('Password and Re-enter Password not same') }}"
			}
		}
	});
});
</script>
@endsection
