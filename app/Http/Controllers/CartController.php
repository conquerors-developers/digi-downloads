<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App;
use App\Http\Requests;
use \Cart as Cart;
use Validator;
use App\Product;
use App\Payment_Items;
use App\Settings;
use App\Payment;
use App\Paypal;
use Auth;
use Input;
use Softon\Indipay\Facades\Indipay;

class CartController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['main_active'] 	= 'products';
		$payment_gateways_record = Settings::where('key', '=', 'payment_gateways')->first();
		
		$data['payment_gateways'] = ['0' => getPhrase('Please select')] + array_pluck( Settings::where('parent_id', '=', $payment_gateways_record->id)->get(), 'title', 'key');
		return view('cart.cart', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
        'id'  => 'required|exists:products',
		'name'  => 'required',
		'price'  => 'required',
        ];
		
		$this->validate($request, $rules);	
		
		$duplicates = Cart::search(function ($cartItem, $rowId) use ($request) {
				return $cartItem->id === $request->id;
			});		

        if (!$duplicates->isEmpty()) {
            return redirect('cart')->withSuccessMessage('Item is already in your cart!');
        }
		$product_details = Product::where('id', '=', $request->id)->first();
		$licence_price = 0;
		if( $request->has('licence') ) {
			$licence = App\Licence::where('id', '=', $request->licence)->first();
			if( $licence ) {
				$licence_price = $licence->price;
			}
		}
		
		if( $product_details->price_type == 'variable' ) {			
			$options = $product_details->price;
			$price_variations = (array) json_decode( $product_details->price_variations );
			
			if( ! empty( $price_variations ) ) {
				$index = 0;
				foreach( $price_variations as $key => $val ) {
					if( in_array( $key, $options ) ) {
						if( $index == 0) {
							$price = $val->amount;
						} else {
							$price = $val->amount;
						}						
						Cart::add($request->id, $product_details->name . ' - ' . $val->name, 1, $price, ['option' => $val->name, 'licence_price' => $licence_price, 'product_id' => $request->id ])->associate('App\Product');
					}
				}
			}
		} else {
			$price = $product_details->price;
			Cart::add($request->id, $product_details->name, 1, $price, ['licence_price' => $licence_price, 'product_id' => $request->id])->associate('App\Product');
		}
		if($request->session()->has('licence_price')) {
			$previoous_value = $request->session()->get('licence_price');
			$request->session()->put('licence_price', $previoous_value + $licence_price);
		} else {
			$request->session()->put('licence_price', $licence_price);
		}
        return redirect('cart')->withSuccessMessage('Item was added to your cart!');
    }
		
	public function paynow(Request $request)
    {
		if( Cart::total() > 0 )
		{
			$rules = [
			'gateway'  => 'required',
			'email'  => 'required|email',
			'first_name'  => 'required|min:5',
			];
			$messages = [
				'gateway.required' => getPhrase('Please select payment gateway'),
				'email.required' => getPhrase('Please enter email address'),
				'email.email' => getPhrase('Please enter valid email address'),
				'first_name.required' => getPhrase('Please select payment gateway')
			];
			$this->validate($request, $rules, $messages);
		}
		
		if( Cart::total() == 0 ) // Free Product Process
		{
			$token = $this->preserveBeforeSave( $request );
			return redirect( URL_CART_PAYMENTSUCCESS );
		}
		else
		{
			$payment_gateway = $request->gateway;
			if($payment_gateway == 'paypal')
			{			
				$paypal = new Paypal();
				$paypal->config['return'] 		= URL_PAYPAL_PAYMENT_SUCCESS.'?token='.$token;
				$paypal->config['cancel_return'] 	= URL_PAYPAL_PAYMENT_CANCEL.'?token='.$token;
				$paypal->invoice = $token;
				
				$paypal->config['email'] = $request->email;
				$paypal->config['first_name'] = $request->first_name;
				$paypal->config['last_name'] = $request->last_name;
				
				foreach (Cart::content() as $item) {
					$paypal->add($item->name, $item->subtotal, $item->qty, $item->rowId); //ADD  item
				}
				
				$token = $this->preserveBeforeSave( $request );
					
				return redirect( $paypal->pay() ); //Proccess the payment
			}
			elseif($payment_gateway == 'payu')
			{
				$token = $this->preserveBeforeSave( $request );

				$config = config();
				$payumoney = $config['indipay']['payumoney'];

				$payumoney['successUrl'] = URL_PAYU_PAYMENT_SUCCESS.'?token='.$token;
				$payumoney['failureUrl'] = URL_PAYU_PAYMENT_CANCEL.'?token='.$token;
				
				$user = Auth::user();
				$parameters = [
					'key' => getSetting('payu_merchant_key','payu'),
					'tid'       => $token,
					'surl'      => URL_PAYU_PAYMENT_SUCCESS.'?token='.$token,
					'furl'      => URL_PAYU_PAYMENT_CANCEL.'?token='.$token,
					'firstname' => $user->first_name,
					'lastname' => $user->last_name,
					'email'     =>$user->email,
					'phone'     => ($user->phone)? $user->phone : '45612345678',
					'productinfo' => 'Products',
					'service_provider'  => 'payu_paisa',				
					'amount'    => Cart::total(),				
				];
				$parameters['curl'] = URL_PAYU_PAYMENT_CANCEL.'?token='.$token;
				$parameters['address1'] = $user->billing_address1;
				$parameters['address2'] = $user->billing_address2;
				$parameters['city'] = $user->billing_city;
				$parameters['state'] = $user->billing_state;
				$parameters['country'] = $user->billing_country;
				$parameters['zipcode'] = $user->billing_zip;
				/*
				$parameters['udf1'] = URL_PAYU_PAYMENT_CANCEL.'?token='.$token;
				$parameters['udf2'] = URL_PAYU_PAYMENT_CANCEL.'?token='.$token;
				$parameters['udf3'] = URL_PAYU_PAYMENT_CANCEL.'?token='.$token;
				$parameters['udf4'] = URL_PAYU_PAYMENT_CANCEL.'?token='.$token;
				$parameters['udf5'] = URL_PAYU_PAYMENT_CANCEL.'?token='.$token;
				$parameters['pg'] = URL_PAYU_PAYMENT_CANCEL.'?token='.$token;
				*/
				
				$parameters['salt'] = getSetting('payu_salt','payu');
				$parameters['testMode'] = getSetting('payu_testmode','payu');
				
	//dd($parameters);
				return Indipay::gateway('PayUMoney')->purchase($parameters);
			}
			elseif($payment_gateway == 'offline-payment')
			{
				$token = $this->preserveBeforeSave( $request );
					
				$data['layout']             = getLayout();
				$data['title'] 	= getPhrase( 'offline_payment' );
				$data['main_active'] 	= 'products';
				$data['token'] = $token;
				return view('cart.offline', $data);
			}
		}
	}
	
	/**
     * This method saves the submitted data from user and waits for the admin approval
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function updateOfflinePayment(Request $request)
    {
      
	  $record = Payment::where('slug', '=', $request->token)->first();
	  
	  if( $record ) {
		  $record->payment_details = $request->payment_details;
		  $record->save();
		  
		Cart::destroy();
		$request->session()->forget('paymenttoken');
		if( $request->session()->has('discount_amount') ) {
				$request->session()->forget('discount_amount');
			}
		  if(Auth::check()) {
			if( checkRole(getUserGrade(6)) )
				return redirect(URL_USERS_DASHBOARD.'/purchases');
			elseif( checkRole(getUserGrade(8)) )
				return redirect(URL_VENDOR_DASHBOARD.'/purchases');
			elseif( checkRole(getUserGrade(3)) )
				return redirect( URL_DASHBOARD );
		  } else {
			  return redirect( URL_CART_PAYMENTSUCCESS )->withErrorMessage('your_payment_success. Byt admin need to accept your payment');
		  }
		  
	  } else {
		flash('Error','invalid_operation', 'error');
		return back();
	  }      
    }
	
	/**
     * This method saves the record before going to payment method
     * The exact record can be identified by using the slug 
     * By using slug we will fetch the record and update the payment status to completed
     * @param  [type] $item           [description]
     * @param  [type] $payment_method [description]
     * @return [type]                 [description]
     */
    public function preserveBeforeSave( $request )
    {
		$user = getUserRecord();
		
		if ($request->session()->has('paymenttoken')) {
			$token = $request->session()->get('paymenttoken', '');
			$payment = Payment::where('slug', '=', $token)->first();
		} else {
			$payment 			= new Payment();
			$request->session()->put('paymenttoken', getHashCode());
		}
		
		if( $user )
		$payment->user_id     = $user->id;
		$payment->payment_gateway = $request->gateway;
		if ($request->session()->has('paymenttoken')) {
			$payment->slug 		= $request->session()->get('paymenttoken', '');
		} else {
			$payment->slug 		= getHashCode();
		}
		
		// $payment->transaction_id = '';
		// $payment->paid_by = '';
		$licence_price = 0;
		if( session()->has('discount_amount') ) {
			$licence_price = session()->get('licence_price');
		}
		$discount_amount = 0;
		if( session()->has('discount_amount') ) {
			$discount_amount = session()->get('discount_amount');
			
		}
		
		$tax = Cart::instance('default')->tax();
		$cart_total = Cart::total();
		$payment->cost 		= $cart_total + $licence_price - $discount_amount; // Item Price + Tax + Licence - $discount_amount
		
		$payment->cart_total = $cart_total; // Item Price only
		$payment->item_price = $cart_total - $tax;
		$payment->tax = $tax;
		$payment->licence_price = $licence_price;
		$payment->discount_amount = $discount_amount;		
		$payment->paid_amount = 0;
		$payment->payment_status = PAYMENT_STATUS_PENDING;
		$other_details = array(
			'cost' => Cart::total() + $licence_price - $discount_amount, // Item Price + Tax + Licence - $discount_amount
			'cart_total' => $cart_total, // Item Price + Tax			
			'item_price' => $cart_total - $tax, // Item Price only
			'tax' => $tax,
			'licence_price' => $licence_price,
			'discount_amount' => $discount_amount,
		);
		$payment->other_details 	= json_encode($other_details);
	
		$payment->customer_email = $request->email;
		$payment->customer_first_name = $request->first_name;
		$payment->customer_last_name = $request->last_name;
		$payment->save();
		
		// Let us save cart items
		foreach (Cart::content() as $item) {
			$payment_item = Payment_Items::where('payment_id', '=', $payment->id)->where('item_slug', '=', $item->model->slug)->first();
			if( ! $payment_item ) {
				$payment_item 	= new Payment_Items();
			}
			$payment_item->payment_id = $payment->id;
			$payment_item->slug = getHashCode();
			$payment_item->item_id = $item->model->id;
			$payment_item->item_slug = $item->model->slug;
			$payment_item->item_name = $item->model->name;
			$payment_item->cost = $item->subtotal;
			$payment_item->other_details = $item->model;
			$payment_item->quantity = $item->qty;
			$payment_item->save();
		}
		return $payment->slug;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        // Validation on max quantity
        $validator = Validator::make($request->all(), [
            'quantity' => 'required|numeric|between:1,5'
        ]);

         if ($validator->fails()) {
            session()->flash('error_message', 'Quantity must be between 1 and 5.');
            return response()->json(['success' => false]);
         }

        Cart::update($id, $request->quantity);
        session()->flash('success_message', 'Quantity was updated successfully!');

        return response()->json(['success' => true]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		//echo sizeof(Cart::content());die();
		if (sizeof(Cart::content()) == 1)
		{
			Cart::destroy();
			session()->forget('licence_price');
			session()->forget('paymenttoken');
			if( session()->has('discount_amount') ) {
				session()->forget('discount_amount');
				session()->forget('discount_details');
			}
		}
		else
		{
		if( session()->has('licence_price') ) {
			$licence_price = session()->get('licence_price');
			$item_licence = 0;
			foreach(Cart::get($id)->options as $key => $val ) {
				$item_licence = $val;
			}
			session()->put('licence_price', $licence_price - $item_licence);
		}
		Cart::remove($id);
		}
		//session()->forget('paymenttoken');
        return redirect('cart')->withSuccessMessage('Item has been removed!');
    }

    /**
     * Remove the resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function emptyCart()
    {
        Cart::destroy();
		session()->forget('licence_price');
		session()->forget('paymenttoken');
		if( $request->session()->has('discount_amount') ) {
			$request->session()->forget('discount_amount');
			$request->session()->forget('discount_details');
		}
        return redirect('cart')->withSuccessMessage('Your cart has been cleared!');
    }

    /**
     * Switch item from shopping cart to wishlist.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function switchToWishlist($id)
    {
        $item = Cart::get($id);

        Cart::remove($id);

        $duplicates = Cart::instance('wishlist')->search(function ($cartItem, $rowId) use ($id) {
            return $cartItem->id === $id;
        });

        if (!$duplicates->isEmpty()) {
            return redirect('cart')->withSuccessMessage('Item is already in your Wishlist!');
        }

        Cart::instance('wishlist')->add($item->id, $item->name, 1, $item->price)
                                  ->associate('App\Product');

        return redirect('cart')->withSuccessMessage('Item has been moved to your Wishlist!');

    }
	
	public function paypal_success(Request $request)
    {
		$response = $request->all();

        $slug = Input::get('token');
		$payment_record = Payment::where('slug', '=', $slug)->first();
		$user = getUserRecord();
    	if( $payment_record )
    	{
			$amount = $request->mc_gross;
			$payment_record->transaction_record = json_encode($response);
			if( $request->payment_status == 'Success' ) {
				$payment_record->payment_status = PAYMENT_STATUS_SUCCESS;
				$payment_record->paid_amount = $amount;
			}
			$payment_record->transaction_id = $request->txn_id;
			$payment_record->save();
			//PAYMENT RECORD UPDATED SUCCESSFULLY
			//PREPARE SUCCESS MESSAGE
			flash('success', 'your_subscription_was_successfull', 'overlay');
			$email_template = 'subscription_success';
			$paymentdetails = Payment::select(['payments.slug as payment_slug','payments.payment_gateway','payments.transaction_id','payments.actual_cost','payments.discount_amount','payments.tax','payments.paid_amount','payments.payment_status','pi.*'])->join('payments_items as pi', 'payments.id', '=', 'pi.payment_id')->where('payments.slug', '=', $slug);
			$products = '<table class="table">
									<thead>
										<tr>
											<th>'.getPhrase('Image').'</th>
											<th>'. getPhrase('Product Name').'</th>
											<th>'. getPhrase('Product Price').'</th>
										</tr>
									</thead>
									<tbody>';
			foreach( $paymentdetails->get() as $item ) {
				$products .= '<tr>
											<td class="table-image">
											';
											
											$product_image = DEFAULT_PRODUCT_IMAGE_THUMBNAIL;
											if( $item->item_image != '' && File::exists(UPLOAD_PATH_PRODUCTS_THUMBNAIL . $item->model->image ) ) {
												$product_image = UPLOAD_URL_PRODUCTS_THUMBNAIL . $item-item_image;
											}
				$products .= '
											<a href="'.URL_DISPLAY_PRODUCTS_DETAILS . $item->item_slug.'"><img src="'.$product_image.'" alt="product" class="img-responsive cart-image"></a></td>
											
											<td><a href="'.URL_DISPLAY_PRODUCTS_DETAILS . $item->item_slug .'">'. $item->item_name .'</a></td>
											
											<td class="colu2">'. currency( $item->cost ) .'</td>											

										</tr>';
			}
			$products .= '</tbody></table>';
			
			$name = $request->first_name;
			$email = $request->email;
			if( ! $name ) {
				$name = $response['first_name'] . ' ' . $response['last_name'];
			}
			if( ! $email ) {
				$email = $response['payer_email'];
			}
			sendEmail($email_template, array('username'=>$name, 
			'cost' => $amount,
			'to_email' => $email,
			'products' => $products,
			));
			// Let us clear the cart
			Cart::destroy();
			$request->session()->forget('paymenttoken');
			if( $request->session()->has('discount_amount') ) {
				$request->session()->forget('discount_amount');
				$request->session()->forget('discount_details');
			}
			if( $user ) {
				if( checkRole(getUserGrade(6)) )
					return redirect(URL_USERS_DASHBOARD.'/purchases')->withSuccessMessage('Thank you for your purchase!');
				elseif( checkRole(getUserGrade(8)) )
					return redirect(URL_VENDOR_DASHBOARD.'/purchases')->withSuccessMessage('Thank you for your purchase!');
				elseif( checkRole(getUserGrade(3)) )
					return redirect( URL_DASHBOARD )->withSuccessMessage('Thank you for your purchase!');
			} else {
				$request->session()->put('purchaseconfirm', $request);
				return redirect( URL_CART_PURCHASECONFIRM )->withSuccessMessage( getPhrase('Thank you for your purchase!') );
			}
			
    	}
    	else {
    	//PAYMENT RECORD IS NOT VALID
    	//PREPARE METHOD FOR FAILED CASE
    	  pageNotFound();
    	}
		//REDIRECT THE USER BY LOADING A VIEW
	
    	return redirect(URL_PAYMENTS_LIST.$user->slug);    	
    }
	
	public function paypal_cancel()
    {

    	if($this->paymentFailed())
    	{
    		//FAILED PAYMENT RECORD UPDATED SUCCESSFULLY
    		//PREPARE SUCCESS MESSAGE
    		  flash('Ooops...!', 'your_payment_was cancelled', 'overlay');
    	}
    	else {
    	//PAYMENT RECORD IS NOT VALID
    	//PREPARE METHOD FOR FAILED CASE
    	  pageNotFound();
    	}

    	//REDIRECT THE USER BY LOADING A VIEW
    	$user = Auth::user();
    	return redirect(URL_PAYMENTS_LIST.$user->slug);
    	 
    }
	
	
	public function show()
	{
		if( Auth::check() )
		{
			$data['main_active'] 	= 'products';
			$payment_gateways_record = Settings::where('key', '=', 'payment_gateways')->first();
			
			$data['payment_gateways'] = Settings::where('parent_id', '=', $payment_gateways_record->id)->get();
			return view('cart.checkout', $data);
		} else {
			return redirect(URL_USERS_LOGIN)->withErrorMessage( 'please login to proceed with check out' );
		}
	}
	
	public function purchaseConfirm()
	{
		if($request->session()->has('purchaseconfirm'))
		{
				$details = $request->session()->has('purchaseconfirm');
				dd($details);
		}
		else
		{
			flash('error', 'wrong operation', 'overlay');
			redirect( URL_DISPLAY_PRODUCTS );
		}
	}
	
	public function payu_success(Request $request)
    {
       $response = $request->all();        
		$slug = Input::get('token');
		$payment_record = Payment::where('slug', '=', $slug)->first();
		$user = getUserRecord();
		if( $payment_record )
		{
			//PAYMENT RECORD UPDATED SUCCESSFULLY
			//PREPARE SUCCESS MESSAGE
			flash('success', 'your_subscription_was_successfull', 'overlay');
			$email_template = 'subscription_success';
			
			$paymentdetails = Payment::select(['payments.slug as payment_slug','payments.payment_gateway','payments.transaction_id','payments.actual_cost','payments.discount_amount','payments.tax','payments.paid_amount','payments.payment_status','pi.*'])->join('payments_items as pi', 'payments.id', '=', 'pi.payment_id')->where('payments.slug', '=', $slug);
			$products = '<table class="table">
									<thead>
										<tr>
											<th>'.getPhrase('Image').'</th>
											<th>'. getPhrase('Product Name').'</th>
											<th>'. getPhrase('Product Price').'</th>
										</tr>
									</thead>
									<tbody>';
			foreach( $paymentdetails->get() as $item ) {
				$products .= '<tr>
											<td class="table-image">
											';
											
											$product_image = DEFAULT_PRODUCT_IMAGE_THUMBNAIL;
											if( $item->item_image != '' && File::exists(UPLOAD_PATH_PRODUCTS_THUMBNAIL . $item->model->image ) ) {
												$product_image = UPLOAD_URL_PRODUCTS_THUMBNAIL . $item-item_image;
											}
				$products .= '
											<a href="'.URL_DISPLAY_PRODUCTS_DETAILS . $item->item_slug.'"><img src="'.$product_image.'" alt="product" class="img-responsive cart-image"></a></td>
											
											<td><a href="'.URL_DISPLAY_PRODUCTS_DETAILS . $item->item_slug .'">'. $item->item_name .'</a></td>
											
											<td class="colu2">'. currency( $item->cost ) .'</td>											

										</tr>';
			}
			$products .= '</tbody></table>';
			
			$name = $request->first_name;
			$email = $request->email;
			if( ! $name ) {
				$name = $response['first_name'] . ' ' . $response['last_name'];
			}
			if( ! $email ) {
				$email = $response['payer_email'];
			}
			$amount = $request->mc_gross;
			sendEmail($email_template, array('username'=>$name, 
			'cost' => $amount,
			'to_email' => $email,
			'products' => $products,)
			);
			// Let us clear the cart
			Cart::destroy();
			$request->session()->forget('paymenttoken');
			if( $request->session()->has('discount_amount') ) {
				$request->session()->forget('discount_amount');
				$request->session()->forget('discount_details');
			}
			if( $user ) {
				if( checkRole(getUserGrade(6)) )
					return redirect(URL_USERS_DASHBOARD.'/purchases')->withSuccessMessage('Thank you for your purchase!');
				elseif( checkRole(getUserGrade(8)) )
					return redirect(URL_VENDOR_DASHBOARD.'/purchases')->withSuccessMessage('Thank you for your purchase!');
				elseif( checkRole(getUserGrade(3)) )
					return redirect( URL_DASHBOARD )->withSuccessMessage('Thank you for your purchase!');
			} else {
				$request->session()->put('purchaseconfirm', $request);
				return redirect( URL_CART_PURCHASECONFIRM )->withSuccessMessage( getPhrase('Thank you for your purchase!') );
			}
		}
        else {
      //PAYMENT RECORD IS NOT VALID
      //PREPARE METHOD FOR FAILED CASE
        pageNotFound();
      }     
      if( $user ) {
		  return redirect('user/mydownloads')->withSuccessMessage('Thank you for your purchase!');
	  } else {
		return redirect( URL_DISPLAY_PRODUCTS );
	  }
    }

     public function payu_cancel(Request $request)
    {
      // dd($request);
      if($this->paymentFailed())
      {
        //FAILED PAYMENT RECORD UPDATED SUCCESSFULLY
        //PREPARE SUCCESS MESSAGE
          flash('Ooops...!', 'your_payment_was cancelled', 'overlay');
      }
      else {
      //PAYMENT RECORD IS NOT VALID
      //PREPARE METHOD FOR FAILED CASE
        pageNotFound();
      }

      //REDIRECT THE USER BY LOADING A VIEW
      $user = Auth::user();
      return redirect(URL_PAYMENTS_LIST.$user->slug);
       
    }
	
	/**
     * Common method to handle payment failed records
     * @return [type] [description]
     */
    protected function paymentFailed()
    {
    	$slug = Input::get('token');
      //dd(Input::get());
    	$payment_record = Payment::where('slug', '=', $slug)->first();
     
     	if(!$this->processPaymentRecord($payment_record))
     	{
    		return FALSE;
     	}
	
		$payment_record->payment_status = PAYMENT_STATUS_CANCELLED;
    	$payment_record->save();
    	
    	return TRUE;
    	 
    }
	
	/**
     * This method Process the payment record by validating through 
     * the payment status and the age of the record and returns boolen value
     * @param  Payment $payment_record [description]
     * @return [type]                  [description]
     */
    protected  function processPaymentRecord(Payment $payment_record)
    {
    	if(!$this->isValidPaymentRecord($payment_record))
    	{
    		flash('Oops','invalid_record', 'error');
    		return FALSE;
    	}

    	if($this->isExpired($payment_record))
    	{
    		flash('Oops','time_out', 'error');
    		return FALSE;
    	}

    	return TRUE;
    }
	
	/**
     * This method validates the payment record before update the payment status
     * @param  [type]  $payment_record [description]
     * @return boolean                 [description]
     */
    protected function isValidPaymentRecord(Payment $payment_record)
    {
    	$valid = FALSE;
    	if($payment_record)
    	{
    		if($payment_record->payment_status == PAYMENT_STATUS_PENDING || $payment_record->payment_gateway=='offline')
    			$valid = TRUE;
    	}
    	return $valid;
    }
	
	public function paymentsuccess(Request $request)
	{
		if( $request->session()->has('paymenttoken') )
		{			
			$token = $request->session()->get('paymenttoken', '');
			$data['layout']             = getLayout();
			if( $status == 'success') {
				$data['title'] 	= getPhrase( 'payment_status : success' );
			} else {
				$data['title'] 	= getPhrase( 'payment_status : pending' );
			}
			
			$data['main_active'] 	= 'products';
			$data['token'] = $token;
			$data['status'] = $status;
			$data['paymentmethod'] = $paymentmethod;
			$data['paymentdetails'] = Payment::select(['payments.*'])->join('payments_items as pi', 'payments.id', '=', 'pi.payment_id')->where('payments.slug', '=', $token)->first();
			if (sizeof(Cart::content()) > 0) {
				$data['cart_details'] = Cart::content();
			}
			// Let us clear the cart
			Cart::destroy();
			$request->session()->forget('paymenttoken');
			if( $request->session()->has('discount_amount') ) {
				$request->session()->forget('discount_amount');
				$request->session()->forget('discount_details');
			}			
			return view('cart.paymentsuccess', $data);
		}
		else
		{
			flash('Oops','wrong_operation', 'error');
			return redirect( URL_DISPLAY_PRODUCTS );
		}
	}
	
	public function applycoupon(Request $request)
	{
		$details = App\Coupon::where('code', '=', $request->coupon)->where('status', '=', 'Active');
		$response = array();
		
		if( $details->count() > 0 ) {
			$start_date = $details->first()->start_date;
			$end_date = $details->first()->end_date;
			$details->whereRaw('"'.date('Y-m-d H:i:s') . '" BETWEEN start_date and end_date');
			if( $details->count() > 0)
			{
				$details = $details->first();
				$product_categories = $cart_product_ids = array();
				foreach (Cart::content() as $item) {
					$cats = ($item->model->categories != '') ? (array) json_decode($item->model->categories) : array();
					$cart_product_ids[] = $item->model->id;
					if( ! empty( $cats ) ) {
						foreach( $cats as $cat ) {
							$product_categories[] = $cat;
						}
					}
				}
				$coupon_categories = ($details->categories) ? (array) json_decode($details->categories) : array();
				if( count( array_intersect( $coupon_categories, $product_categories ) ) ) {
					$exclude_products = ($details->exclude_products) ? (array) json_decode($details->exclude_products) : array();
					if( count( array_intersect( $exclude_products, $cart_product_ids ) ) ) {
						$response['status'] = 0;
						$response['message'] = getPhrase('sorry!! this coupon is not applicable for this product');
					} else {
						// Success
						$minimum_amount = $details->minimum_amount;
						if( Cart::total() >= $minimum_amount) {
							$type = $details->type;
							$value = $details->value;
							$discount = 0;
							if( $type == 'percent' ) {
								$discount = ((Cart::total() - Cart::instance('default')->tax()) * $value) / 100; // We are calculating discount on actual produt price.
							} else {
								$discount = $value;
							}
							$request->session()->put('discount_amount', $discount);
							$request->session()->put('discount_details', $details);
							$response['status'] = 1;
							$code = $details->code;
							$details_msg = '<div class="coupon-apply">
			<p class="coupon-data"><span class="fa fa-check"></span>'. getPhrase('Coupon Code') . $code . ' ' . currency( $discount ) . getPhrase(' applied') .'</p>
			<p class="coupon-data1">'. currency( $discount ) . getPhrase(' reduced from the cart') .'</p>
			<p><a href="'. URL_CART_REMOVECOUPON .'"><span class="fa fa-times"></span></a></p>
		</div>';
							$response['message'] = '<div class="alert alert-success">' . getPhrase('Wah!! coupon applied successfully') . '</div>' . $details_msg;
							
						} else {
							$response['status'] = 0;
							$response['message'] = getPhrase('sorry!! you need to purchase at least ') . currency( $minimum_amount ) . getPhrase( ' to apply this coupon' );
						}						
					}
				} else {
					// Coupon is not in categories
					$response['status'] = 0;
					$response['message'] = getPhrase('sorry!! this coupon is not applicable for this category of products');
				}
			} else {
				$response['status'] = 0;
				$response['message'] = getPhrase('sorry!! coupon code has expired');
			}
		} else {
			$response['status'] = 0;
			$response['message'] = getPhrase('sorry!! coupon_not_found');
		}
		return json_encode($response);
	}
	
	public function removecoupon()
	{
		if( session()->has('discount_amount') ) {
			session()->forget('discount_amount');
			session()->forget('discount_details');
		}
		return redirect( URL_CHECKOUT )->withSuccessMessage( getPhrase( 'coupon_has_been_removed_from_the_cart' ) );
	}
}
