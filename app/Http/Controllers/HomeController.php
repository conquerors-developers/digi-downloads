<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$user = getUserRecord();		
		if( $user->confirmed == 0 )
		{
			flash('Ooops...!', 'Please confirm your account to login', 'error');
			return view('common.logout');
		}
		if( $user->role_id == USER_ROLE_ID ) {
			return redirect( URL_USERS_DASHBOARD );
		} elseif( $user->role_id == VENDOR_ROLE_ID ) {
			return redirect( URL_VENDOR_DASHBOARD );
		}
		$data['layout']          = getLayout();
		$data['main_active']    = 'dashboard';
		$data['sub_active']     = 'dashboard';
		$data['title']          = 'Dashboard';
        return view('admin.dashboard', $data);
    }
}
