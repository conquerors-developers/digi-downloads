<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use \Auth;
use App\User;
use \Cart as Cart;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    //use AuthenticatesUsers;
	use AuthenticatesUsers {
		logout as performLogout;
	}

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }
	
	public function logout(Request $request)
	{
		$this->performLogout($request);
		flash('Wah...!', getPhrase('Hope you enjoyed the shopping with us. Visit again.'), 'success');
		return redirect(URL_USERS_LOGIN);
	}
	
	public function confirm($confirmation_code)
	{
		$record = User::where('confirmation_code', $confirmation_code)->first();
		if($isValid = $this->isValidRecord($record))
		return redirect($isValid);

		$record->confirmed = 1;
		$record->confirmation_code = null;
		$record->status = 'Active';
		$record->save();
		flash('Success', getPhrase("You have successfully activated your account. Please login here."), 'success');
		return redirect(URL_USERS_LOGIN);
	}
	
	public function isValidRecord($record)
	{
	  if ($record === null) {
			flash('Ooops...!', getPhrase("code not valid"), 'error');
			return $this->getRedirectUrl();
		}
	}
	
	public function performLogin(Request $request)
	{
		$login_status = FALSE;
		if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'confirmed' => 1, 'status' => 'Active'])) {
			$login_status = TRUE;
		}
		if(!$login_status) 
        {
               $record = User::where('email', '=', $request->email)->first();
			   $message = getPhrase("We have not found email address entered");
			   if( $record )
			   {
				   if( $record->confirmed == 0 )
				   {
					   $message = getPhrase('Please check your email to activate your acount');
				   }
				   elseif($record->status == 'Suspended' )
				   {
					   $message = getPhrase('We are sorry!. Your account has beed suspended. Please contact administrator');
				   }
				   else
				   {
					   $message = getPhrase('Please check email address OR password either one is wrong');
				   }
			   }
			   
			   flash('Ooops...!', $message, 'error');
			   return redirect()->back();
        }
		
		if($login_status)
        {
			$user = getUserRecord();
			if( in_array($user->role_id, array( OWNER_ROLE_ID, ADMIN_ROLE_ID, EXECUTIVE_ROLE_ID )) )
			{
				return redirect($this->redirectTo);
			}
			elseif( $user->role_id == USER_ROLE_ID )
			{
				if (sizeof(Cart::content()) > 0) {
					return redirect( URL_CHECKOUT );
				} else {
					return redirect( URL_USERS_DASHBOARD );
				}
			}
			else
			{
				return redirect($this->redirectTo);
			}
			
		}
	}
}
