<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Pages;
use Newsletter;

class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['main_active']    = 'home';
        $data['sub_active']     = 'dashboard';
        $data['title']          = 'Dashboard';
		$urlparams = $request->all();
		$category = $sub_category = '';
		if( isset($urlparams['category']) ) {
			$category = $urlparams['category'];
		}
		if( isset($urlparams['sub-category']) ) {
			$sub_category = $urlparams['sub-category'];
		}
		$data['selected_category']     = $category;
        $data['selected_sub_category'] = $sub_category;
		$params = array();
		if( $request->ajax() ) 
		{			
			if( $request->param == 'latest' ) {
				$params['orderby'] = 'latest';
			}
			if( $request->param == 'free' ) {
				$params['free'] = 'free';
			}
			if( $request->param == 'popular' ) {
				$params['popular'] = 'popular';
			}
			if( $request->param == 'featured' ) {
				$params['featured'] = 'featured';
			}
		}
		$data['products'] = Product::getProducts( $params )->paginate( 3 );
		$data['category_products'] = Product::getProducts( $params )->paginate( PRODUCTS_DISPLAY_SIZE );
		if ($request->ajax()) {
			return view('displayproducts.products', ['products' => $data['products']]);
		}
        return view('welcome', $data);
    }
	
	public function getProducts(Request $request)
	{
		
	}
	
	public function searchProduct(Request $request)
	{
		$params = '?';
		if( $request->title != '' )
		{
			$params .= 'title=' . $request->title;
		}
		if( $request->category != '0' )
		{
			$params .= '&category=' . $request->category;
		}
		if( $params == '?')
		return redirect( URL_DISPLAY_PRODUCTS );
		else
		return redirect( URL_DISPLAY_PRODUCTS . $params );
	}
	
	public function subscribe(Request $request)
	{
		$rules = [
			'email'  => 'required|email',
		];
		$this->validate($request, $rules);
		Newsletter::subscribeOrUpdate($request->email);
		$response = '';
		if( Newsletter::lastActionSucceeded() )
		{
			$response = '<div class="alert alert-success">'.getPhrase('Thanks for your subscription') . '</div>';
		}
		else
		{
			$response = '<div class="alert alert-success">'.getPhrase( Newsletter::getLastError() ) . '</div>';
		}
		echo $response;
	}
	
	public function page( Pages $slug )
	{		
		if( ! $slug )
		{
			flash('Oops','Page not found', 'error');
			return redirect( PREFIX );
		}
		
		$data['main_active']    = 'home';
        $data['sub_active']     = 'dashboard';
        $data['title']          = 'Dashboard';
		$data['record'] = $slug;
		return view('page', $data);
		
	}
}
