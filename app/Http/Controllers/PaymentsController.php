<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Payment;
use App\Payment_Items;
use App\Product;
use App\Payment_Items_Downloads;
use Yajra\Datatables\Datatables;
use DB;
use Illuminate\Support\Facades\Hash;
use Excel;
use Input;
use File;
use Exception;
use Auth;
use Image;

class PaymentsController extends Controller
{
	 /**
     * This method redirects the user to view the onlinepayments reports dashboard
     * It contains an optional slug, if slug is null it will redirect the user to dashboard
     * If the slug is success/failed/cancelled/all it will show the appropriate result based on slug status from payments table
     * @param  string $slug [description]
     * @return [type]       [description]
     */
    public function onlinePaymentsReport()
    {

      if(!checkRole(getUserGrade(2)))
      {
        prepareBlockUserMessage();
        return back();
      }


      $data['main_active']        = 'reports';
      $data['sub_active']         = 'online_reports';
      $data['title']              = getPhrase('online_payments');
      $data['payments']           = (object)$this->prepareSummaryRecord('online');
      // $data['payments_chart_data']= (object)$this->getPaymentStats($data['payments']);
      // $data['payments_monthly_data'] = (object)$this->getPaymentMonthlyStats();
      $data['payment_mode']        = 'online';
      $data['layout']              = getLayout();
      return view('payments.reports.payment-report', $data);  
    }


    /**
     * This method list the details of the records
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function listOnlinePaymentsReport($slug)
    {
      if(!checkRole(getUserGrade(2)))
      {
        prepareBlockUserMessage();
        return back();
      }
      if(!in_array($slug, ['all','pending', 'success','cancelled']))
      {
        pageNotFound();
        return back();
      }

      $payment = new Payment();
       $this->updatePaymentTransactionRecords($payment->updateTransactionRecords('online'));

         $data['main_active']        = 'reports';
      $data['sub_active']            = 'online_reports';
        $data['payments_mode']      = getPhrase('online_payments');
        if($slug=='all'){
           $data['title']              = getPhrase('all_payments');
       
        }
        elseif($slug=='success'){
        $data['title']              = getPhrase('success_list');
          }
        elseif($slug=='pending'){
        $data['title']              = getPhrase('pending_list');
          }
       elseif($slug='cancelled'){
           $data['title']              = getPhrase('cancelled_list');
         }
        $data['layout']             = getLayout();
        $data['ajax_url']           = URL_ONLINE_PAYMENT_REPORT_DETAILS_AJAX.$slug;
        $data['payment_mode']       = 'online';
        return view('payments.reports.payments-report-list', $data);   
    }


     public function updatePaymentTransactionRecords($records)
    {

      foreach($records as $record)
      {
        $rec = Payment::where('id',$record->id)->first();
        $this->isExpired($rec);
      }
    }

     public function getOnlinePaymentReportsDatatable($slug)
    {

      if(!checkRole(getUserGrade(2)))
      {
        prepareBlockUserMessage();
        return back();
      }
    
     $records = Payment::join('users', 'users.id','=','payments.user_id')
                        ->join('payments_items','payments_items.payment_id','=','payments.id')

     ->select(['users.image', 'users.name',  'payments_items.item_id','payments.cost','paid_amount','payment_gateway','payments.updated_at','payment_status','payments.id'])
     ->where('payment_gateway', '!=', 'offline-payment')
     ->orderBy('updated_at', 'desc');
     if($slug!='all')
      $records->where('payment_status', '=', $slug);
      return Datatables::of($records)
      
        ->editColumn('payment_status',function($records){

          $rec = '';
          if($records->payment_status==PAYMENT_STATUS_CANCELLED)
           $rec = '<span class="label label-danger">'.ucfirst($records->payment_status).'</span>';
          elseif($records->payment_status==PAYMENT_STATUS_PENDING)
            $rec = '<span class="label label-info">'.ucfirst($records->payment_status).'</span>';
          elseif($records->payment_status==PAYMENT_STATUS_SUCCESS)
            $rec = '<span class="label label-success">'.ucfirst($records->payment_status).'</span>';
          return $rec;
        })
        ->editColumn('image', function($records) {
           return '<img src="'.getProfilePath($records->image).'"  /> '; 
        })
        ->editColumn('name', function($records)
        {
          return ucfirst($records->name);
        })

         ->editColumn('cost', function($records)
        {
          $product_details = Product::where('id','=',$records->item_id)->first();
           
           $product_owner  = User::where('id','=',$product_details->user_created)->first();

           return ucfirst($product_owner->name);

        })

         ->editColumn('item_id', function($records)
        {
          $product_details = Product::where('id','=',$records->item_id)->first();
          
          return ucfirst($product_details->name);
        })
        
       ->editColumn('payment_gateway', function($records)
        {
          $text =  ucfirst($records->payment_gateway);

         if($records->payment_status==PAYMENT_STATUS_SUCCESS) {
          $extra = '<ul class="list-unstyled payment-col clearfix"><li>'.$text.'</li>';
          $extra .='<li><p>Cost:'.$records->cost.'</p><p>Aftr Dis.:'.$records->after_discount.'</p><p>Paid:'.$records->paid_amount.'</p></li></ul>';
          return $extra;
        }
          return $text;
        })
        ->removeColumn('id')
        ->make();     
    }

     /**
     * This method redirects the user to view the onlinepayments reports dashboard
     * It contains an optional slug, if slug is null it will redirect the user to dashboard
     * If the slug is success/failed/cancelled/all it will show the appropriate result based on slug status from payments table
     * @param  string $slug [description]
     * @return [type]       [description]
     */
    public function offlinePaymentsReport()
    {
      if(!checkRole(getUserGrade(2)))
      {
        prepareBlockUserMessage();
        return back();
      }

      $data['main_active']        = 'reports';
      $data['sub_active']         = 'offline_reports';
      $data['title']              = getPhrase('offline_payments');
      $data['payments']           = (object)$this->prepareSummaryRecord('offline');
      // $data['payments_chart_data'] = (object)$this->getPaymentStats($data['payments']);
      // $data['payments_monthly_data'] = (object)$this->getPaymentMonthlyStats('offline', '=');
      $data['payment_mode']       = 'offline';
      $data['layout']             = getLayout();
       return view('payments.reports.payment-report', $data);  
    }


     /**
     * This method list the details of the records
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function listOfflinePaymentsReport($slug)
    {
      if(!in_array($slug, ['all','pending', 'success','cancelled']))
      {
        pageNotFound();
        return back();
      }


         $data['main_active']        = 'reports';
         $data['sub_active']         = 'offline_reports';
        $data['payments_mode']      = getPhrase('offline_payments');
         if($slug=='all'){
           $data['title']              = getPhrase('all_payments');
       
        }
        elseif($slug=='success'){
        $data['title']              = getPhrase('success_list');
          }
        elseif($slug=='pending'){
        $data['title']              = getPhrase('pending_list');
          }
       elseif($slug='cancelled'){
           $data['title']              = getPhrase('cancelled_list');
         }   
        $data['layout']             = getLayout();
        $data['ajax_url']           = URL_OFFLINE_PAYMENT_REPORT_DETAILS_AJAX.$slug;
        $data['payment_mode']       = 'offline';

        return view('payments.reports.payments-report-list', $data);   
    }

    /**
     * This method gets the list of records 
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function getOfflinePaymentReportsDatatable($slug)
    {
      if(!checkRole(getUserGrade(2)))
      {
        prepareBlockUserMessage();
        return back();
      }
    
    $records = Payment::join('users', 'users.id','=','payments.user_id')
                        ->join('payments_items','payments_items.payment_id','=','payments.id')

     ->select(['users.image', 'users.name',  'payments_items.item_id','payments.cost','paid_amount','payment_gateway','payments.updated_at','payment_status','payments.id'])
     ->where('payment_gateway', '=', 'offline-payment')
     ->orderBy('updated_at', 'desc');
     if($slug!='all')
      $records->where('payment_status', '=', $slug);
      return Datatables::of($records)
      
        ->editColumn('payment_status',function($records){

          $rec = '';
          if($records->payment_status==PAYMENT_STATUS_CANCELLED)
           $rec = '<span class="label label-danger">'.ucfirst($records->payment_status).'</span>';
          elseif($records->payment_status==PAYMENT_STATUS_PENDING) {
            $rec = '<span class="label label-info">'.ucfirst($records->payment_status).'</span>&nbsp;<button class="btn btn-primary btn-sm" onclick="viewDetails('.$records->id.');">'.getPhrase('view_details').'</button>';
          }
          elseif($records->payment_status==PAYMENT_STATUS_SUCCESS)
            $rec = '<span class="label label-success">'.ucfirst($records->payment_status).'</span>';
          return $rec;
        })
        ->editColumn('image', function($records){
           return '<img src="'.getProfilePath($records->image).'"  /> '; 
        })
        ->editColumn('name', function($records)
        {
          return ucfirst($records->name);
        })
         ->editColumn('payment_gateway', function($records)
        {
          $text =  ucfirst($records->payment_gateway);

         if($records->payment_status==PAYMENT_STATUS_SUCCESS) {
          $extra = '<ul class="list-unstyled payment-col clearfix"><li>'.$text.'</li>';
          $extra .='<li><p>Cost:'.$records->cost.'</p><p>Aftr Dis.:'.$records->after_discount.'</p><p>Paid:'.$records->paid_amount.'</p></li></ul>';
          return $extra;
        }
          return $text;
        })
        
        ->removeColumn('id')
        ->removeColumn('action')
        ->make();     
    }
   

    public function getPaymentRecord(Request $request)
        {

        if(!checkRole(getUserGrade(2)))
            {
              prepareBlockUserMessage();
              return back();
            }
        $payment_record = Payment::where('id','=',$request->record_id)->first();
$date_format = getSetting('date_format', 'site_settings');
		   $html = '<div class="row">
           <div class="col-md-8 col-md-offset-2">
               <p><strong>'.getPhrase('name').'</strong> : '.$payment_record->customer_first_name.' ' . $payment_record->customer_last_name . '</p>
               <p><strong>'.getPhrase('cost').'</strong> :  '.currency( $payment_record->cost ).'</p>';
			   if( $payment_record->licence_price != '0' ) {
			   $html .= '
			   <p><strong>'.getPhrase('licence_price').'</strong> :  '.currency( $payment_record->licence_price ).'</p>';
			   }
			   
			   if( $payment_record->tax != '0' ) {
			   $html .= '
			   <p><strong>'.getPhrase('tax').'</strong> :  '.currency( $payment_record->tax ).'</p>';
			   }
			   
			   if( $payment_record->discount_amount != '0' ) {
               $html .= '<p><strong>'.getPhrase('discount').'</strong> : '.currency( $payment_record->discount_amount ).'</p>';
			   }
               
               $html .= '<p><strong>'.getPhrase('notes').'</strong> :  '.$payment_record->payment_details.'</p>
               <p><strong>'.getPhrase('created_at').'</strong> : '.date($date_format, strtotime($payment_record->created_at)).'</p>
               <p><strong>'.getPhrase('updated_at').'</strong> : '.date($date_format, strtotime($payment_record->updated_at)).'</p>
               <p><strong>'.getPhrase('comments').'</strong> : <textarea class="form-control" name="admin_comment"></textarea></p>
               <input type="hidden" name="record_id" value="'.$payment_record->id.'">
           </div>
        </div>';
        return $html;

        // $result['status'] = 0;
        // $result['record'] = null;
        // if($payment_record)
        // {
        //   $result['status'] = 1;
        //   $result['record'] = $payment_record;
        // }
        // return json_encode($result);
      }
 
    /**
     * This method prepares different variations of reports based on the type
     * This is a common method to prepare online, offline and overall reports
     * @param  string $type [description]
     * @return [type]       [description]
     */
    public function prepareSummaryRecord($type='overall')
    {

      $payments = [];
      if($type=='online') {
        $payments['all'] = $this->getRecordsCount('online');

        $payments['success'] = $this->getRecordsCount('online', 'success');
        $payments['cancelled'] = $this->getRecordsCount('online', 'cancelled');
        $payments['pending'] = $this->getRecordsCount('online', 'pending');
      }
      else if($type=='offline') {
        $payments['all'] = $this->getRecordsCount('offline');

        $payments['success'] = $this->getRecordsCount('offline', 'success');
        $payments['cancelled'] = $this->getRecordsCount('offline', 'cancelled');
        $payments['pending'] = $this->getRecordsCount('offline', 'pending');
      }

      return $payments;
    }

     /**
     * This is a helper method for fetching the data and preparing payment records count
     * @param  [type] $type   [description]
     * @param  string $status [description]
     * @return [type]         [description]
     */
    public function getRecordsCount($type, $status='')
    {
      $count = 0;
      if($type=='online') {
        if($status=='')
          $count = Payment::where('payment_gateway', '!=', 'offline-payment')->count();

        else
        {
          $count = Payment::where('payment_gateway', '!=', 'offline-payment')
                            ->where('payment_status', '=', $status)
                            ->count();
        }
      }      
      else if($type=='offline')
      {
         if($status=='')
          $count = Payment::where('payment_gateway', '=', 'offline-payment')->count();

        else
        {
          $count = Payment::where('payment_gateway', '=', 'offline-payment')
                            ->where('payment_status', '=', $status)
                            ->count();
        } 
      }


      return $count;
    }

    /**
     * This method is used to approve or reject the request
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function approveOfflinePayment(Request $request)
    {

      $payment_record = Payment::where('id','=',$request->record_id)->first();
      try{
      if($request->submit == 'approve')
      {
        $this->approvePayment($payment_record, $request);
      }
      else {
        $user = getUserRecord($payment_record->user_id);
        sendEmail('offline_subscription_failed', array('username'=>$user->name, 
          'plan' => $payment_record->plan_type,
          'to_email' => $user->email, 'admin_comment'=>$request->admin_comment));

         $payment_record->payment_status = PAYMENT_STATUS_CANCELLED;
         $payment_record->admin_comments = $request->admin_comment;
        
        $payment_record->save();
      }

      flash('success', 'record_was_updated_successfully', 'overlay');
    }
    catch(Exception $ex){

      $message = $ex->getMessage();
      flash('Oops..!', $message, 'overlay');
    }
      return redirect(URL_OFFLINE_PAYMENT_REPORTS);
    }


    public function approvePayment(Payment $payment_record,Request $request ,$iscoupon_zero = 0)
{
  
      

        if($payment_record->plan_type == 'combo')
        {
          $item_model = new ExamSeries(); 
        }
        

        if($payment_record->plan_type == 'exam') {
          $item_model = new Quiz();
        }

        if($payment_record->plan_type == 'lms') {
          $item_model = new LmsSeries();
        }
        
        $item_details = $item_model->where('id', '=',$payment_record->item_id)->first();
         
        $daysToAdd = '+'.$item_details->validity.'days';

        $payment_record->start_date = date('Y-m-d');
        $payment_record->end_date = date('Y-m-d', strtotime($daysToAdd));
        
        $details_before_payment         = (object)json_decode($payment_record->other_details);
        $payment_record->coupon_applied = $details_before_payment->is_coupon_applied;
        $payment_record->coupon_id      = $details_before_payment->coupon_id;
        $payment_record->actual_cost    = $details_before_payment->actual_cost;
        $payment_record->discount_amount= $details_before_payment->discount_availed;
        $payment_record->after_discount = $details_before_payment->after_discount;
        $payment_record->paid_amount = $details_before_payment->after_discount;
        if(!$iscoupon_zero)
          $payment_record->admin_comments = $request->admin_comment;

        $payment_record->payment_status = PAYMENT_STATUS_SUCCESS;
        
        $user = getUserRecord($payment_record->user_id);

        $email_template = 'offline_subscription_success';

        if($iscoupon_zero){
          $email_template = 'subscription_success';
          sendEmail($email_template, array('username'=>$user->name, 
          'plan' => $payment_record->plan_type,
          'to_email' => $user->email));

        }
        else {
          sendEmail($email_template, array('username'=>$user->name, 
          'plan' => $payment_record->plan_type,
          'to_email' => $user->email, 'admin_comment'=>$request->admin_comment));
        }

         $payment_record->save();

        if($payment_record->coupon_applied)
        {
          $this->couponcodes_usage($payment_record);
        }

        return TRUE;
}

public function couponcodes_usage($payment_record)
    {
          $coupon_usage['user_id'] = $payment_record->user_id;
          $coupon_usage['item_id'] = $payment_record->item_id;
          $coupon_usage['item_type'] = $payment_record->plan_type;
          $coupon_usage['item_cost'] = $payment_record->actual_cost;
          $coupon_usage['total_invoice_amount'] = $payment_record->paid_amount;
          $coupon_usage['discount_amount'] = $payment_record->discount_amount;
          $coupon_usage['coupon_id'] = $payment_record->coupon_id;
          $coupon_usage['updated_at'] =  new \DateTime();
          DB::table('couponcodes_usage')->insert($coupon_usage);
          return TRUE;
    }

}