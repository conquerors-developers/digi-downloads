<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Payment;
use App\Payment_Items;
use App\Product;
use App\Payment_Items_Downloads;
use Yajra\Datatables\Datatables;
use DB;
use Illuminate\Support\Facades\Hash;
use Excel;
use Input;
use File;
use Exception;
use Auth;
use Image;

class UsersController extends Controller
{
    public function __construct()
    {
         $this->middleware('auth');
    }

     /**
    * Display a listing of the resource.
    *
    * @return Response
    */
     public function index( $type = 'all' )
     {
     
        if(!checkRole(getUserGrade(2)))
        {
          prepareBlockUserMessage();
          return back();
        }
 
      	$data['layout']      	= getLayout();
		$data['main_active'] 	= 'users';
        if( $type == 'all' ) {
			$data['sub_active']     = 'list';
		} else {
			$data['sub_active']     = $type . 'list';
		}
		$data['title']        	= getPhrase( $type . ' Users' );
		$data['type']        	= $type;

        return view('users.list', $data);
     }


    /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    
    public function getDatatable($slug = '')
    {
        $records = array();      
		if( $slug == 'all' ) {
			$records = User::join('roles', 'users.role_id', '=', 'roles.id')
			->select(['users.name', 'email', 'roles.display_name','role_id',
			'image','slug', 'users.id', 'users.updated_at'])
			->orderBy('users.updated_at', 'desc')
			->get();
		} else {
			$role = getRoleData($slug);
			$records = User::join('roles', 'users.role_id', '=', 'roles.id')
			->select(['users.name', 'email', 'roles.display_name','role_id',
			'image','slug', 'users.id', 'users.updated_at'])
			->where('role_id', '=', $role)
			->orderBy('users.updated_at', 'desc')
			->get();
		}
        return Datatables::of($records)
        ->addColumn('action', function ($records) {
           
          $role = getRoleData($records->role_id);

          
         
          $link_data =  '<a href="'.URL_USERS_EDIT.$records->slug.'" class="btn btn-warning"><i class="fa fa-edit"></i></a> ';

          if($role == 'vendor'){

             $link_data .=  '<a href="'.URL_USERS_VENDOR_DETAILS.$records->slug.'" class="btn btn-primary"><i class="fa fa-info-circle" aria-hidden="true"></i></a> ';

          }

          elseif ($role == 'user') {
            
            $link_data .=  '<a href="'.URL_USERS_CUSTOMER_DETAILS.$records->slug.'" class="btn btn-primary"><i class="fa fa-info-circle" aria-hidden="true"></i></a> ';
           
          }

          
          $temp='';

          //Show delete option to only the owner user
          if(checkRole(getUserGrade(1)) && $records->id!=\Auth::user()->id)   {
          $temp = '<a class="btn btn-danger" href="javascript:void(0);" onclick="deleteRecord(\''.$records->slug.'\');"><i class="fa fa-trash"></i></a>';
           } 
              
          $temp .='</ul> </div>';
          $link_data .= $temp;
            return $link_data;
            })
         ->editColumn('name', function($records) {

           $role = getRoleData($records->role_id);
          if($role =='vendor')
            return '<a href="'.URL_USERS_VENDOR_DETAILS.$records->slug.'">'.ucfirst($records->name).'</a>';
          else if($role=='user')
          {
            
              return '<a href="'.URL_USERS_CUSTOMER_DETAILS.$records->slug.'">'.ucfirst($records->name).'</a>';
          }
            return ucfirst($records->name);
        })        
         ->editColumn('image', function($records){
          $image_path = DEFAULT_USERS_IMAGE_THUMBNAIL;
          if($records->image)
            $image_path = IMAGE_PATH_USERS_THUMBNAIL.$records->image;
            return '<img height="45" width="45" class="image" src="'.$image_path.'" />';
         })
        
 
        
        ->removeColumn('role_id')
        ->removeColumn('id')
        ->removeColumn('slug')
        ->removeColumn('updated_at')
         
        ->make();
    }

    /**
     *This method will load the user creation page
     * @return [type] [description]
     */
    public function create()
    {
        if(!checkRole(getUserGrade(2)))
        {
          prepareBlockUserMessage();
          return back();
        }

    	$data['record']      	= FALSE;
        $data['layout']      	= getLayout();
        $data['main_active'] 	= 'users';
        $data['sub_active']     = 'add';
        $data['title']        	= 'Add User';
        $data['roles']    		= \App\Role::pluck('display_name', 'id');
        	
        return view('users.add-edit', $data);
    }

    /**
     * This meserthod will add the user to the table and attaches the 
     * permissions for that user
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request)
    {
       if(!checkRole(getUserGrade(2)))
        {
          prepareBlockUserMessage();
          return back();
        }
    	$this->validate($request, [
        'first_name'  => 'bail|required|max:20|',
        'email'	=> 'bail|required|email|unique:users,email',
        'password'     => 'bail|required|min:3',
        'role_id'  => 'bail|required|',
        'image'     => 'bail|mimes:png,jpg,jpeg|max:2048',
        ]);
         DB::beginTransaction();
    	$role_name = '';
		try {
	        $user 			= new User();
	        $name           = $request->first_name;
			if( $request->last_name != '' )
			 $name .= ' ' . $request->last_name;
	        $user->name 	= $name;
	        $user->slug 	= $user->makeSlug($name);
	        $user->email 	= $request->email;
	        $user->password	= bcrypt($request->password);
			$user->role_id  = $request->role_id;
	        
			$user->first_name = $request->first_name;
			 $user->last_name = $request->last_name;			 
             $user->billing_address1 = $request->billing_address1;
			 $user->billing_address2 = $request->billing_address2;
			 $user->billing_city = $request->billing_city;
			 $user->billing_zip = $request->billing_zip;
			 $user->billing_state = $request->billing_state;
			 $user->billing_country = $request->billing_country;
			 $user->about_me = $request->about_me;
			 $user->social_links = json_encode( $request->social_links );
			 
	        $user->save();

	       $user->roles()->attach($user->role_id);

            $role_name = getRoleData($user->role_id);

	       	flash('Success','Record updated successfully', 'success');
	       	  DB::commit();
   		}
   		catch(Exception $ex) {
   			  DB::rollBack();
   				flash('Oops',$ex->getMessage(), 'overlay');
   		}

   		if( $role_name == '' ) {
			return redirect(URL_USERS);
		} else {
			return redirect(URL_USERS . '/' . $role_name);
		}


    }

    /**
     * This module will load the edit view page
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function edit($slug)
    {
       if(!checkRole(getUserGrade(2)))
        {
          prepareBlockUserMessage();
          return back();
        }
        $user_record = User::where('slug','=',$slug)->first();
		//dd($user_record);
		if( ! $user_record )
		{
			flash('Oops','user_not_found', 'overlay');
			return redirect(URL_USERS);
		}
        
        $data['record']         = $user_record;
        $data['layout']         = getLayout();
        $data['main_active']    = 'users';
        $data['sub_active']     = 'add';
        $data['title']          = 'Add User';
        $data['roles']          = \App\Role::pluck('display_name', 'id');
            
        return view('users.add-edit', $data);
    }

    /**
     * This method will update the submitted data
     * @param  Request $request [description]
     * @param  [type]  $slug    [description]
     * @return [type]           [description]
     */
    public function update(Request $request, $slug)
    {
        if(!checkRole(getUserGrade(2)))
        {
          prepareBlockUserMessage();
          return back();
        }

		$role_name = '';
       $user = User::where('slug','=',$slug)->first();
        $this->validate($request, [
			'first_name'  => 'bail|required|max:20|',
			'email'     => 'bail|required|unique:users,email,'.$user->id,
			'image'     => 'bail|mimes:png,jpg,jpeg|max:2048',
			'role_id'  => 'bail|required|'
        ]);

 
        $role_id = $user->role_id;
         DB::beginTransaction();
         $name           = $request->first_name;
		 if( $request->last_name != '' )
			 $name .= ' ' . $request->last_name;
		 
         if($user->name != $name)
            $user->slug = $user->makeSlug($name);

        if($request->password)
            $user->password  = bcrypt($request->password);


        try {
            
            $user->name     = $name;
            
			$user->email    = $request->email;
			$user->role_id  = $request->role_id;
			 
			 $user->first_name = $request->first_name;
			 $user->last_name = $request->last_name;			 
             $user->billing_address1 = $request->billing_address1;
			 $user->billing_address2 = $request->billing_address2;
			 $user->billing_city = $request->billing_city;
			 $user->billing_zip = $request->billing_zip;
			 $user->billing_state = $request->billing_state;
			 $user->billing_country = $request->billing_country;
			 $user->about_me = $request->about_me;
			 $user->social_links = json_encode( $request->social_links );

            $user->save();


          if($role_id != $request->role_id)
          {
               DB::table('role_user')
              ->where('user_id', '=', $user->id)
              ->where('role_id', '=', $role_id)
              ->delete();
          
       
           $user->roles()->attach($user->role_id); 
          }
		  $role_name = getRoleData($user->role_id);
          $this->processUpload($request, $user);
           
            flash('Success','Record updated successfully', 'success');
              DB::commit();
        }
        catch(Exception $ex) {
              DB::rollBack();
                flash('Oops',$ex->getMessage(), 'overlay');
        }
		if( $role_name == '' ) {
			return redirect(URL_USERS);
		} else {
			return redirect(URL_USERS . $role_name);
		}
    }



     public function processUpload(Request $request, User $user)
     {
         if ($request->hasFile('image')) {
          
          $old_image = $user->image;

          $destinationPath      = UPLOAD_PATH_USERS;
          $destinationPathThumb = UPLOAD_PATH_USERS_THUMBNAIL;
          
          $fileName = $user->id.'.'.$request->image->guessClientExtension();
          ;
          $request->file('image')->move($destinationPath, $fileName);

          $user->image = $fileName;
         
          Image::make($destinationPath.$fileName)->fit(160)->save($destinationPath.$fileName);
         
          Image::make($destinationPath.$fileName)->fit(45)->save($destinationPathThumb.$fileName);
          $user->save();

            $this->deleteFile($old_image, UPLOAD_PATH_USERS);
            $this->deleteFile($old_image, UPLOAD_PATH_USERS_THUMBNAIL);
        }
     }

    /**
     * Delete Record based on the provided slug
     * @param  [string] $slug [unique slug]
     * @return Boolean 
     */
    public function delete($slug)
    {
         if(!checkRole(getUserGrade(2)))
        {
          prepareBlockUserMessage();
          return back();
        }
        
        $record = User::where('slug', $slug)->first();
        $role = getRoleData($record->role_id);
        /**
         * Check if user is having any pending records in library
         * If the user is staff, check if the user is allocated to any course-subject
         */
        DB::beginTransaction();
        try{

          $image = $record->image;
            $record->delete();

            $this->deleteFile($image, UPLOAD_PATH_USERS);
            $this->deleteFile($image, UPLOAD_PATH_USERS_THUMBNAIL);

             DB::commit();
            $response['status'] = 1;
            $response['message'] = getPhrase('record_deleted_successfully');
      
      
          }
          catch (Exception $e) {
             DB::rollBack();
              $response['status'] = 0;
              $response['message'] =  $e->getMessage();
           
         }
          return json_encode($response);
      
    }

    /**
     * This method will delete the file at specified path
     * @param  [type]  $record   [description]
     * @param  [type]  $path     [description]
     * @param  boolean $is_array [description]
     * @return [type]            [description]
     */
    public function deleteFile($record, $path, $is_array = FALSE)
    {
        $files = array();
        $files[] = $path.$record;
        File::delete($files);
    }
    
    /**
     * This method display the users dashboard in admin view
     * @return [type] [description]
     */
    public function adminDashBoard()
    {   
        if(!checkRole(getUserGrade(2)))
        {
          prepareBlockUserMessage();
          return back();
        }
        $data['layout']       = getLayout();
        $data['main_active']  = 'users';
        $data['sub_active']   = 'all';
          
        return view('users.users-dashboard', $data);


    }
     
    /**
     * This method display the vendor dashborad
     * @param  string $value [description]
     * @return [type]        [description]
     */
    public function vendorDetails($user_slug)
    { 
        $record = User::where('slug','=',$user_slug)->first();
        
        $total_uploads = Product::where('user_created',$record->id)->get();
        $vendor_uploads = $total_uploads->count();
        if(!count($vendor_uploads)){
          $vendor_uploads = 0;
        }

        $purchase_items = Payment::where('user_id',$record->id)->get()->count();
        if(!count($purchase_items)){
          $purchase_items = 0;
        }

        $vendor_got_amount = Payment_Items::join('payments','payments.id','=','payment_id')
                                          ->join('products','products.id','=','item_id')
                                          ->where('products.user_created','=',$record->id)
                                          ->select('payments.paid_amount')->get();

        $data['layout']       = getLayout();
        $data['main_active']  = 'users';
        $data['record']       = $record;
        $data['vendor_uploads']  = $vendor_uploads;
        $data['total_uploads']   = $total_uploads;
        $data['purchase_items']  = $purchase_items;
        $data['vendor_got_amount']  = $vendor_got_amount;
        $data['sub_active']   = 'vendorlist';
        $data['title']        = $record->name.' '.getPhrase('dashborad');

        return view('users.details.vendor-profile', $data);
         
    }

    /**
     * This method display the vendor upload products
     * @param  string $value [description]
     * @return [type]        [description]
     */
    public function vendorUploadProducts($user_slug)
    {
      
        $record = User::where('slug','=',$user_slug)->first();
        $data['layout']       = getLayout();
        $data['main_active']  = 'users';
        $data['record']       = $record;
        $data['sub_active']   = 'vendorlist';
        $data['title']        = $record->name.' '.getPhrase('uploads');

        return view('products.vendor.upload', $data);

    }
     /**
      * This menthod get the vendor upload details
      * @param  [type] $user_slug [description]
      * @return [type]            [description]
      */
    public function vendorUploadProductsList($user_slug)
    {
       
      $record = User::where('slug','=',$user_slug)->first();

      $records = array(); 
    DB::statement(DB::raw('set @rownum=0'));
    $records = Product::select([ DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id','name','slug', 'price', 
                                 'image', 'status'])
                        ->where('user_created','=',$record->id);
    if( current_user_role() == VENDOR_ROLE_ID ) {
      $records->where('user_created', '=', current_user_id());
    }
    
    return Datatables::of($records)
   
    ->editColumn('status', function($records) {
      return ucfirst($records->status);
    })
    ->editColumn('name',function($records){

      return '<a href="'.URL_PRODUCT_DETAILS.$records->id.'">'.ucfirst($records->name).'</a>';
    })
    ->removeColumn('id')
    ->removeColumn('slug')
    ->make(); 

    }

    /**
     * This method display sales list of specifice vendor products
     * @param  [type] $user_slug [description]
     * @return [type]            [description]
     */
    public function vendorProductsSales($user_slug)
    {
      
       $record = User::where('slug','=',$user_slug)->first();
        $data['layout']       = getLayout();
        $data['main_active']  = 'users';
        $data['record']       = $record;
        $data['user_slug']    = $user_slug;
        $data['sub_active']   = 'vendorlist';
        $data['title']        = $record->name.' '.getPhrase('product_sales');

        return view('products.vendor.sales', $data);


    }

   /**
    * This Method get the sales list of specifice vendor products
    * @param  string $value [description]
    * @return [type]        [description]
    */
    public function vendorProductsSalesList($user_slug)
    {
       
     $record = User::where('slug','=',$user_slug)->first();

    $records = array(); 
   $records = Payment_Items::join('payments','payments.id','=','payment_id')
                                          ->join('products','products.id','=','item_id')
                                          ->where('products.user_created','=',$record->id)
                                          ->select(['products.name','products.price','payments.user_id','payments.coupon_applied','payments.paid_amount','payments.payment_gateway','payments.payment_status','payments_items.created_at','payments.customer_email'])
                                          ->orderby('payments_items.updated_at','desc');;
   
    return Datatables::of($records)
   
    ->editColumn('payment_status', function($records) {
      return ucfirst($records->status);
    })
    ->editColumn('user_id',function($records){

       $user_data = User::where('id','=',$records->user_id)->first();
       return $user_data->name;
    })

     ->editColumn('coupon_applied',function($records){
        
        if($records->coupon_applied==0){
          return 'No';
        }
        
        return 'Yes';
    })
    ->make(); 

    }
   

    /**
     * This method display sales list of specifice vendor purchases
     * @param  [type] $user_slug [description]
     * @return [type]            [description]
     */
    public function vendorPurchases($user_slug)
    {
      
       $record = User::where('slug','=',$user_slug)->first();
        $data['layout']       = getLayout();
        $data['main_active']  = 'users';
        $data['record']       = $record;
        $data['user_slug']    = $user_slug;
        $data['sub_active']   = 'vendorlist';
        $data['title']        = $record->name.' '.getPhrase('purchases');

        return view('products.vendor.purchases', $data);


    }

   /**
    * This Method get the sales list of specifice vendor purchases
    * @param  string $value [description]
    * @return [type]        [description]
    */
    public function vendorPurchasesLists($user_slug)
    {
       
     $record = User::where('slug','=',$user_slug)->first();

    $records = array(); 
   $records = Payment::join('payments_items','payments_items.payment_id','=','payments.id')
                      ->join('products','products.id','=','payments_items.item_id')
                      ->where('payments.user_id','=',$record->id)
                      ->select(['products.name','products.price','coupon_applied','paid_amount','payment_gateway','payment_status','payments_items.created_at','user_id'])
                      ->orderby('payments_items.updated_at','desc');
   
    return Datatables::of($records)
   
    ->editColumn('payment_status', function($records) {
      return ucfirst($records->status);
    })
    ->editColumn('user_id',function($records){

       $user_data = User::where('id','=',$records->user_id)->first();
       return $user_data->email;
    })

     ->editColumn('coupon_applied',function($records){
        
        if($records->coupon_applied==0){
          return 'No';
        }
        
        return 'Yes';
    })
    ->make(); 

    }


     /**
     * This method display the customer dashborad
     * @param  string $value [description]
     * @return [type]        [description]
     */
    public function customersProfile($user_slug)
    { 
        $record = User::where('slug','=',$user_slug)->first();
        
        $purchase_items = Payment::where('user_id',$record->id)->get()->count();
        if(!count($purchase_items)){
          $purchase_items = 0;
        }
        
        $customer_downloads = Payment_Items_Downloads::join('payments','payments.id','=','payment_id')
                                ->where('payments.user_id','=',$record->id)
                                ->get()->count();

        if(!count($customer_downloads)){
          $customer_downloads = 0;
        }                        
       
        $total_amount = Payment::join('payments_items','payments_items.payment_id','=','payments.id')
                          ->where('payments.user_id','=',$record->id)
                          ->select('paid_amount')->get();


        $data['layout']       = getLayout();
        $data['main_active']  = 'users';
        $data['record']       = $record;
        $data['purchase_items']  = $purchase_items;
        $data['total_amount']    = $total_amount;
        $data['customer_downloads']  = $customer_downloads;
        $data['sub_active']   = 'userlist';
        $data['title']        = $record->name.' '.getPhrase('dashborad');

        return view('users.details.customer-profile', $data);
         
    }


    /**
     * This method display sales list of specifice customer purchases
     * @param  [type] $user_slug [description]
     * @return [type]            [description]
     */
    public function customersPurchases($user_slug)
    {
      
       $record = User::where('slug','=',$user_slug)->first();
        $data['layout']       = getLayout();
        $data['main_active']  = 'users';
        $data['record']       = $record;
        $data['user_slug']    = $user_slug;
        $data['sub_active']   = 'userlist';
        $data['title']        = $record->name.' '.getPhrase('purchases');

        return view('products.customers.purchases', $data);


    }

   /**
    * This Method get the sales list of specifice customer purchases
    * @param  string $value [description]
    * @return [type]        [description]
    */
    public function customersPurchasesList($user_slug)
    {
       
     $record = User::where('slug','=',$user_slug)->first();

    $records = array(); 
   $records = Payment::join('payments_items','payments_items.payment_id','=','payments.id')
                      ->join('products','products.id','=','payments_items.item_id')
                      ->where('payments.user_id','=',$record->id)
                      ->select(['products.name','products.price','coupon_applied','paid_amount','payment_gateway','payment_status','payments_items.created_at','user_id'])
                      ->orderby('payments_items.updated_at','desc');
   
    return Datatables::of($records)
   
    ->editColumn('payment_status', function($records) {
      return ucfirst($records->status);
    })
    ->editColumn('user_id',function($records){

       $user_data = User::where('id','=',$records->user_id)->first();
       return $user_data->email;
    })

     ->editColumn('coupon_applied',function($records){
        
        if($records->coupon_applied==0){
          return 'No';
        }
        
        return 'Yes';
    })
    ->make(); 

    }

     /**
     * This method display sales list of specifice customer downloads
     * @param  [type] $user_slug [description]
     * @return [type]            [description]
     */
    public function customersDownloads($user_slug)
    {
      
       $record = User::where('slug','=',$user_slug)->first();
        $data['layout']       = getLayout();
        $data['main_active']  = 'users';
        $data['record']       = $record;
        $data['user_slug']    = $user_slug;
        $data['sub_active']   = 'userlist';
        $data['title']        = $record->name.' '.getPhrase('downloads');

        return view('products.customers.downloads', $data);


    }

   /**
    * This Method get the sales list of specifice customer downloads
    * @param  string $value [description]
    * @return [type]        [description]
    */
    public function customersDownloadsList($user_slug)
    {
       
     $record = User::where('slug','=',$user_slug)->first();

    $records = array(); 
    $records = Payment_Items_Downloads::join('payments','payments.id','=','payment_id')
                                       ->join('payments_items','payments_items.payment_id','=','payments.id')
                                       ->join('products','products.id','=','payments_items.item_id')
                                     ->where('payments.user_id','=',$record->id)
                                     ->select(['products.name','payments_items_downloads.created_at'])
                                     ->orderby('payments_items_downloads.updated_at','desc');
   
    return Datatables::of($records)
   
    ->make(); 

    }
}